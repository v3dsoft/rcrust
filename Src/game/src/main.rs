#![windows_subsystem = "windows"]

mod app;

fn main() {
    let mut app = match app::App::new() {
        Ok(app) => app,
        Err(err) => panic!(format!("Unable to create app object.\nError: {}", err.explain()))
    };

    match app.run() {
        Ok(_) => {},
        Err(err) => panic!(format!("Unhandled error: {}", err.explain()))
    }
}
