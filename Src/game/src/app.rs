use raycast;
use raycast::error;
use raycast::math;
use raycast::scene;
use raycast::utils::gs;

use std::rc::Rc;
use std::cell::RefCell;
use raycast::math::Point3D;

struct TestGS {
    engine: raycast::EngineRC,
    scene: raycast::scene::Scene,

    camera: raycast::render::scene::Camera
}

impl TestGS {
    pub fn new(engine: Rc<RefCell<raycast::Engine>>) -> error::Result<TestGS> {
        let fov_v_angle = math::fov_v_angle_from_h(85.0f32.to_radians(), 16.0/9.0);

        let aspect = {
            let engine = engine.borrow();
            let render_params = engine.render_driver().params();

            (render_params.resolution.width as f32) / (render_params.resolution.height as f32)
        };

        let scene = Self::build_test_map(&engine)?;
        let camera = scene.create_camera_dir(
            &math::Point3D{ x: 300.0, y: 900.0 , z: 175.0 },
            &math::Vector3D{ x: 1.83675, y: -3.55336, z: 0.0},
            fov_v_angle,
            aspect
        )?;

        Ok(TestGS {
            engine,
            scene,

            camera
        })
    }

    fn build_test_map(engine: &Rc<RefCell<raycast::Engine>>) -> error::Result<scene::Scene> {
        const TEST_MAP: &'static [&'static str] = &[
            "******",
            "*s   *",
            "*   **",
            "**   *",
            "***  *",
            "******"
        ];

        let map_w = TEST_MAP[0].len() as u32;
        let map_h = TEST_MAP.len() as u32;

        let mut scene = raycast::Engine::build_scene(engine.clone(), map_w, map_h)?;

        let mut x = 0;
        let mut y = 0;
        for row in TEST_MAP {
            for cell in row.chars() {
                if cell != '*' {
                    scene.set_cell(
                        x, y,
                        scene::Cell::Space {
                            floor: 0.0,
                            ceil: 200.0
                    })?;
                }

                x += 1;
            }

            x = 0;
            y += 1;
        }

        scene.create()
    }
}

impl gs::GameState for TestGS {
    fn update(&mut self, time_delta: u32) -> gs::GSChange {
        let new_angle = self.camera.h_angle() + 5.0f32.to_radians() * (time_delta as f32 * 0.001);
        self.camera.set_h_angle(new_angle);

        gs::GSChange::Same
    }

    fn render(&mut self) -> error::Result<()> {
        self.scene.render(&self.camera)
    }
}

pub struct App {
    engine: raycast::EngineRC,
    gs_proc: gs::GSProcessor
}

impl App {
    pub fn new() -> error::Result<App> {
        let engine = raycast::Engine::build().
            render_resolution(&raycast::engine::Resolution {
                width: 640,
                height: 480
            }).
//            render_fullscreen(true).
//            render_vsync(true).
            create()?;

        let mut gs_proc = gs::GSProcessor::new();
        gs_proc.set_state(Some(Box::new(
            TestGS::new(engine.clone())?
        )));

        Ok(App {
            engine,
            gs_proc
        })
    }

    pub fn run(&mut self) -> error::Result<()> {
        loop {
            if !self.engine.borrow_mut().update() {
                break;
            }

            if !self.gs_proc.update() {
                break;
            }

            self.engine.borrow_mut().render_driver_mut().clear(true, true);
            self.gs_proc.render()?;
            self.engine.borrow_mut().present();
        }

        Ok(())
    }
}