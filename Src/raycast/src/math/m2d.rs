use std::cmp;
use std::ops;
use std::f32;

use super::func::{
    EPSILON,
    F32Funcs
};

#[derive(Debug, Clone)]
pub struct Point2D {
    pub x: f32,
    pub y: f32
}

impl Point2D {
    pub fn distance(p1: &Point2D, p2: &Point2D) -> f32 {
        let dvec = p2 - p1;
        dvec.length()
    }

    pub fn distance_sq(p1: &Point2D, p2: &Point2D) -> f32 {
        let dvec = p2 - p1;
        dvec.length_sq()
    }
}

impl cmp::PartialEq for Point2D {
    fn eq(&self, other: &Self) -> bool {
        self.x.approx_eq(other.x) && self.y.approx_eq(other.y)
    }
}
impl cmp::Eq for Point2D {}

impl ops::Add<&Vector2D> for &Point2D {
    type Output = Point2D;

    fn add(self, rhs: &Vector2D) -> Point2D {
        Point2D {
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

impl ops::AddAssign<&Vector2D> for Point2D {
    fn add_assign(&mut self, rhs: &Vector2D) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl ops::Sub<&Vector2D> for &Point2D {
    type Output = Point2D;

    fn sub(self, rhs: &Vector2D) -> Point2D {
        Point2D {
            x: self.x - rhs.x,
            y: self.y - rhs.y
        }
    }
}

impl ops::SubAssign<&Vector2D> for Point2D {
    fn sub_assign(&mut self, rhs: &Vector2D) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl ops::Sub<&Point2D> for &Point2D {
    type Output = Vector2D;

    fn sub(self, rhs: &Point2D) -> Vector2D {
        Vector2D {
            x: self.x - rhs.x,
            y: self.y - rhs.y
        }
    }
}

#[derive(Debug, Clone)]
pub struct Vector2D {
    pub x: f32,
    pub y: f32
}

impl Vector2D {
    pub fn from_basis(u: &Vector2D, v: &Vector2D, x: f32, y: f32) -> Vector2D {
        &(u * x) + &(v * y)
    }

    pub fn angle(v1: &Self, v2: &Self) -> f32 {
        let len1 = v1.length();
        debug_assert!(len1 > EPSILON, format!("Vector 1 length should be greater than 0.\nVector 1: {:?}", v1));

        let len2 = v2.length();
        debug_assert!(len2 > EPSILON, format!("Vector 2 length should be greater than 0.\nVector 1: {:?}", v2));

        let dot = v1 * v2;

        (dot / (len1 * len2)).acos()
    }

    pub fn angle_full(v1: &Self, v2: &Self) -> f32 {
        let len1 = v1.length();
        debug_assert!(len1 > EPSILON, format!("Vector 1 length should be greater than 0.\nVector 1: {:?}", v1));

        let len2 = v2.length();
        debug_assert!(len2 > EPSILON, format!("Vector 2 length should be greater than 0.\nVector 1: {:?}", v2));

        let v1_perp = Vector2D { x: -v1.y, y: v1.x};

        let dot = v1 * v2;
        let dot_perp = &v1_perp * v2;

        let angle = (dot / (len1 * len2)).acos();

        if dot_perp > 0.0 {
            if dot > 0.0 {
                angle
            } else {
                f32::consts::FRAC_PI_2 + angle
            }
        } else {
            if dot > 0.0 {
                2.0 * f32::consts::PI - angle
            } else {
                f32::consts::PI + angle
            }
        }
    }

    pub fn length(&self) -> f32 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn length_sq(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }

    pub fn normalize(&self) -> Self {
        let inv_len = self.length_sq().rsqrt();

        if inv_len > EPSILON {
            Self {
                x: self.x * inv_len,
                y: self.y * inv_len
            }
        } else {
            Self {
                x: 0.0,
                y: 0.0
            }
        }
    }

    pub fn normalize_this(&mut self) {
        let inv_len = self.length_sq().rsqrt();

        if inv_len > EPSILON {
            self.x *= inv_len;
            self.y *= inv_len;
        } else {
            self.x = 0.0;
            self.y = 0.0;
        }
    }

    pub fn perp_ccw(&self) -> Vector2D {
        Vector2D {
            x: -self.y,
            y: self.x
        }
    }

    pub fn perp_cw(&self) -> Vector2D {
        Vector2D {
            x: self.y,
            y: -self.x
        }
    }
}

impl cmp::PartialEq for Vector2D {
    fn eq(&self, other: &Vector2D) -> bool {
        self.x.approx_eq(other.x) && self.y.approx_eq(other.y)
    }
}
impl cmp::Eq for Vector2D {}

impl ops::Add<&Vector2D> for &Vector2D {
    type Output = Vector2D;

    fn add(self, rhs: &Vector2D) -> Vector2D {
        Vector2D {
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

impl ops::AddAssign<&Vector2D> for Vector2D {
    fn add_assign(&mut self, rhs: &Vector2D) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl ops::Sub<&Vector2D> for &Vector2D {
    type Output = Vector2D;

    fn sub(self, rhs: &Vector2D) -> Vector2D {
        Vector2D {
            x: self.x - rhs.x,
            y: self.y - rhs.y
        }
    }
}

impl ops::SubAssign<&Vector2D> for Vector2D {
    fn sub_assign(&mut self, rhs: &Vector2D) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl ops::Mul<f32> for &Vector2D {
    type Output = Vector2D;

    fn mul(self, rhs: f32) -> Vector2D {
        Vector2D {
            x: self.x * rhs,
            y: self.y * rhs
        }
    }
}

impl ops::MulAssign<f32> for Vector2D {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl ops::Mul for &Vector2D {
    type Output = f32;

    fn mul(self, rhs: &Vector2D) -> f32 {
        self.x * rhs.x + self.y * rhs.y
    }
}

#[derive(Debug, Clone)]
pub struct Matrix2D {
    pub m11: f32,
    pub m12: f32,
    pub m13: f32,

    pub m21: f32,
    pub m22: f32,
    pub m23: f32,

    pub m31: f32,
    pub m32: f32,
    pub m33: f32
}

impl Matrix2D {
    pub fn new(m11: f32, m12: f32, m13: f32,
               m21: f32, m22: f32, m23: f32,
               m31: f32, m32: f32, m33: f32) -> Matrix2D
    {
        Self {
            m11, m12, m13,
            m21, m22, m23,
            m31, m32, m33
        }
    }

    pub fn identity() -> Matrix2D {
        Matrix2D::new(
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0
        )
    }

    pub fn translation(d_pos: &Vector2D) -> Matrix2D {
        Matrix2D::new(
            1.0, 0.0, d_pos.x,
            0.0, 1.0, d_pos.y,
            0.0, 0.0, 1.0
        )
    }

    pub fn rotation(angle: f32) -> Matrix2D {
        let cos_a = angle.cos();
        let sin_a = angle.sin();

        Matrix2D::new(
            cos_a, -sin_a, 0.0,
            sin_a, cos_a, 0.0,
            0.0, 0.0, 1.0
        )
    }

    pub fn scaling(sx: f32, sy: f32) -> Matrix2D {
        Matrix2D::new(
            sx, 0.0, 0.0,
            0.0, sy, 0.0,
            0.0, 0.0, 1.0
        )
    }

    pub fn transpose(&self) -> Matrix2D {
        Matrix2D::new(
            self.m11, self.m21, self.m31,
            self.m12, self.m22, self.m32,
            self.m13, self.m23, self.m33
        )
    }

    pub fn determinant(&self) -> f32 {
        self.m11 * self.m22 * self.m33 +
            self.m12 * self.m23 * self.m31 +
            self.m13 * self.m21 * self.m32 -
            self.m13 * self.m22 * self.m31 -
            self.m21 * self.m12 * self.m33 -
            self.m11 * self.m23 * self.m32
    }

    pub fn invert(&self) -> Matrix2D {
        let sub11 = self.m22 * self.m33 - self.m23 * self.m32;
        let sub12 = self.m21 * self.m33 - self.m23 * self.m31;
        let sub13 = self.m21 * self.m32 - self.m22 * self.m31;
        let sub21 = self.m12 * self.m33 - self.m13 * self.m32;
        let sub22 = self.m11 * self.m33 - self.m13 * self.m31;
        let sub23 = self.m11 * self.m32 - self.m12 * self.m31;
        let sub31 = self.m12 * self.m23 - self.m13 * self.m22;
        let sub32 = self.m11 * self.m23 - self.m13 * self.m21;
        let sub33 = self.m11 * self.m22 - self.m12 * self.m21;

        let det = self.m11 * sub11 - self.m12 * sub12 + self.m13 * sub13;
        let inv_det = 1.0 / det;

        let m11 = sub11 * inv_det;
        let m12 = -sub21 * inv_det;
        let m13 = sub31 * inv_det;
        let m21 = -sub12 * inv_det;
        let m22 = sub22 * inv_det;
        let m23 = -sub32 * inv_det;
        let m31 = sub13 * inv_det;
        let m32 = -sub23 * inv_det;
        let m33 = sub33 * inv_det;

        Matrix2D {
            m11, m12, m13,
            m21, m22, m23,
            m31, m32, m33
        }
    }
}

impl cmp::PartialEq for Matrix2D {
    fn eq(&self, other: &Matrix2D) -> bool {
        self.m11.approx_eq(other.m11) && self.m12.approx_eq(other.m12) && self.m13.approx_eq(other.m13) &&
            self.m21.approx_eq(other.m21) && self.m22.approx_eq(other.m22) && self.m23.approx_eq(other.m23) &&
            self.m31.approx_eq(other.m31) && self.m32.approx_eq(other.m32) && self.m33.approx_eq(other.m33)
    }
}
impl cmp::Eq for Matrix2D {}

impl ops::Mul<&Matrix2D> for &Matrix2D {
    type Output = Matrix2D;

    fn mul(self, rhs: &Matrix2D) -> Matrix2D {
        let m11 = self.m11 * rhs.m11 + self.m12 * rhs.m21 + self.m13 * rhs.m31;
        let m12 = self.m11 * rhs.m12 + self.m12 * rhs.m22 + self.m13 * rhs.m32;
        let m13 = self.m11 * rhs.m13 + self.m12 * rhs.m23 + self.m13 * rhs.m33;

        let m21 = self.m21 * rhs.m11 + self.m22 * rhs.m21 + self.m23 * rhs.m31;
        let m22 = self.m21 * rhs.m12 + self.m22 * rhs.m22 + self.m23 * rhs.m32;
        let m23 = self.m21 * rhs.m13 + self.m22 * rhs.m23 + self.m23 * rhs.m33;

        let m31 = self.m31 * rhs.m11 + self.m32 * rhs.m21 + self.m33 * rhs.m31;
        let m32 = self.m31 * rhs.m12 + self.m32 * rhs.m22 + self.m33 * rhs.m32;
        let m33 = self.m31 * rhs.m13 + self.m32 * rhs.m23 + self.m33 * rhs.m33;

        Matrix2D {
            m11, m12, m13,
            m21, m22, m23,
            m31, m32, m33
        }
    }
}

impl ops::MulAssign<&Matrix2D> for Matrix2D {
    fn mul_assign(&mut self, rhs: &Matrix2D) {
        let m11 = self.m11 * rhs.m11 + self.m12 * rhs.m21 + self.m13 * rhs.m31;
        let m12 = self.m11 * rhs.m12 + self.m12 * rhs.m22 + self.m13 * rhs.m32;
        let m13 = self.m11 * rhs.m13 + self.m12 * rhs.m23 + self.m13 * rhs.m33;

        let m21 = self.m21 * rhs.m11 + self.m22 * rhs.m21 + self.m23 * rhs.m31;
        let m22 = self.m21 * rhs.m12 + self.m22 * rhs.m22 + self.m23 * rhs.m32;
        let m23 = self.m21 * rhs.m13 + self.m22 * rhs.m23 + self.m23 * rhs.m33;

        let m31 = self.m31 * rhs.m11 + self.m32 * rhs.m21 + self.m33 * rhs.m31;
        let m32 = self.m31 * rhs.m12 + self.m32 * rhs.m22 + self.m33 * rhs.m32;
        let m33 = self.m31 * rhs.m13 + self.m32 * rhs.m23 + self.m33 * rhs.m33;

        self.m11 = m11;
        self.m12 = m12;
        self.m13 = m13;

        self.m21 = m21;
        self.m22 = m22;
        self.m23 = m23;

        self.m31 = m31;
        self.m32 = m32;
        self.m33 = m33;
    }
}

impl ops::Mul<&Point2D> for &Matrix2D {
    type Output = Point2D;

    fn mul(self, rhs: &Point2D) -> Point2D {
        let mut x = self.m11 * rhs.x + self.m12 * rhs.y + self.m13;
        let mut y = self.m21 * rhs.x + self.m22 * rhs.y + self.m23;
        let w = self.m31 * rhs.x + self.m32 * rhs.y + self.m33;

        if !w.approx_eq(1.0) {
            let rev_w = 1.0 / w;

            x *= rev_w;
            y *= rev_w;
        }

        Point2D { x, y }
    }
}

impl ops::Mul<&Vector2D> for &Matrix2D {
    type Output = Vector2D;

    fn mul(self, rhs: &Vector2D) -> Vector2D {
        let mut x = self.m11 * rhs.x + self.m12 * rhs.y;
        let mut y = self.m21 * rhs.x + self.m22 * rhs.y;

        Vector2D { x, y }
    }
}

#[derive(Clone)]
pub struct Ray2D {
    pub origin: Point2D,
    pub dir: Vector2D
}

impl Ray2D {
    pub fn new(origin: &Point2D, dir: &Vector2D) -> Ray2D {
        Ray2D {
            origin: origin.clone(),
            dir: dir.clone()
        }
    }

    pub fn from_target(origin: &Point2D, target: &Point2D) -> Ray2D {
        let dir = target - origin;

        Ray2D {
            origin: origin.clone(),
            dir
        }
    }

    pub fn point(&self, t: f32) -> Point2D {
        &self.origin + &(&self.dir * t)
    }

    pub fn clip_h_line(&self, y: f32) -> Option<Point2D> {
        let t = (y - self.origin.y) / self.dir.y;

        if t > -EPSILON {
            Some(self.point(t))
        } else {
            None
        }
    }

    pub fn clip_v_line(&self, x: f32) -> Option<Point2D> {
        let t = (x - self.origin.x) / self.dir.x;

        if t > -EPSILON {
            Some(self.point(t))
        } else {
            None
        }
    }
}

#[derive(Clone)]
pub struct AABox2D {
    min: Point2D,
    max: Point2D
}

impl AABox2D {
    pub fn new(origin: &Point2D, size: f32) -> AABox2D {
        if size > 0.0 {
            AABox2D {
                min: origin.clone(),
                max: Point2D {
                    x: origin.x + size,
                    y: origin.y + size
                }
            }
        } else {
            AABox2D {
                min: Point2D {
                    x: origin.x + size,
                    y: origin.y + size
                },
                max: origin.clone()
            }
        }
    }

    pub fn min(&self) -> &Point2D {
        &self.min
    }

    pub fn max(&self) -> &Point2D {
        &self.max
    }
}
