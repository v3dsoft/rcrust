use std::cmp;
use std::ops;

use crate::error;

use super::func::{
    EPSILON,
    F32Funcs
};

use super::m2d::{
    Point2D,
    Vector2D
};

#[derive(Debug, Clone)]
pub struct Point3D {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Point3D {
    pub fn distance(p1: &Point3D, p2: &Point3D) -> f32 {
        let dvec = p2 - p1;
        dvec.length()
    }

    pub fn distance_sq(p1: &Point3D, p2: &Point3D) -> f32 {
        let dvec = p2 - p1;
        dvec.length_sq()
    }

    pub fn discard_x(&self) -> Point2D {
        Point2D {
            x: self.y,
            y: self.z
        }
    }

    pub fn discard_y(&self) -> Point2D {
        Point2D {
            x: self.x,
            y: self.z
        }
    }

    pub fn discard_z(&self) -> Point2D {
        Point2D {
            x: self.x,
            y: self.y
        }
    }
}

impl cmp::PartialEq for Point3D {
    fn eq(&self, other: &Point3D) -> bool {
        self.x.approx_eq(other.x) &&
        self.y.approx_eq(other.y) &&
        self.z.approx_eq(other.z)
    }
}
impl cmp::Eq for Point3D {}

impl ops::Neg for &Point3D {
    type Output = Point3D;

    fn neg(self) -> Point3D {
        Point3D {
            x: -self.x,
            y: -self.y,
            z: -self.z
        }
    }
}

impl ops::Add<&Vector3D> for &Point3D {
    type Output = Point3D;

    fn add(self, rhs: &Vector3D) -> Point3D {
        Point3D {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z
        }
    }
}

impl ops::AddAssign<&Vector3D> for Point3D {
    fn add_assign(&mut self, rhs: &Vector3D) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl ops::Sub<&Vector3D> for &Point3D {
    type Output = Point3D;

    fn sub(self, rhs: &Vector3D) -> Point3D {
        Point3D {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z
        }
    }
}

impl ops::SubAssign<&Vector3D> for Point3D {
    fn sub_assign(&mut self, rhs: &Vector3D) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl ops::Sub<&Point3D> for &Point3D {
    type Output = Vector3D;

    fn sub(self, rhs: &Point3D) -> Vector3D {
        Vector3D {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z
        }
    }
}

#[derive(Debug, Clone)]
pub struct Vector3D {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Vector3D {
    pub fn angle(v1: &Vector3D, v2: &Vector3D) -> f32 {
        let len1 = v1.length();
        debug_assert!(len1 > EPSILON, format!("Vector 1 length should be greater than 0.\nVector 1: {:?}", v1));

        let len2 = v2.length();
        debug_assert!(len2 > EPSILON, format!("Vector 2 length should be greater than 0.\nVector 1: {:?}", v2));

        let dot = v1 * v2;

        (dot / (len1 * len2)).acos()
    }

    pub fn length(&self) -> f32 {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    pub fn length_sq(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn normalize(&self) -> Vector3D {
        let inv_len = self.length_sq().rsqrt();

        if inv_len > EPSILON {
            Vector3D {
                x: self.x * inv_len,
                y: self.y * inv_len,
                z: self.z * inv_len
            }
        } else {
            Vector3D {
                x: 0.0,
                y: 0.0,
                z: 0.0
            }
        }
    }

    pub fn normalize_this(&mut self) {
        let inv_len = self.length_sq().rsqrt();

        if inv_len > EPSILON {
            self.x *= inv_len;
            self.y *= inv_len;
            self.z *= inv_len;
        } else {
            self.x = 0.0;
            self.y = 0.0;
            self.z = 0.0;
        }
    }

    pub fn cross(v1: &Vector3D, v2: &Vector3D) -> Vector3D {
        Vector3D {
            x: v1.y * v2.z - v1.z * v2.y,
            y: v1.z * v2.x - v1.x * v2.z,
            z: v1.x * v2.y - v1.y * v2.x
        }
    }

    pub fn discard_x(&self) -> Vector2D {
        Vector2D {
            x: self.y,
            y: self.z
        }
    }

    pub fn discard_y(&self) -> Vector2D {
        Vector2D {
            x: self.x,
            y: self.z
        }
    }

    pub fn discard_z(&self) -> Vector2D {
        Vector2D {
            x: self.x,
            y: self.y
        }
    }
}

impl cmp::PartialEq for Vector3D {
    fn eq(&self, other: &Vector3D) -> bool {
        self.x.approx_eq(other.x) &&
        self.y.approx_eq(other.y) &&
        self.z.approx_eq(other.z)
    }
}
impl cmp::Eq for Vector3D {}

impl ops::Neg for &Vector3D {
    type Output = Vector3D;

    fn neg(self) -> Vector3D {
        Vector3D {
            x: -self.x,
            y: -self.y,
            z: -self.z
        }
    }
}

impl ops::Add<&Vector3D> for &Vector3D {
    type Output = Vector3D;

    fn add(self, rhs: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z
        }
    }
}

impl ops::AddAssign<&Vector3D> for Vector3D {
    fn add_assign(&mut self, rhs: &Vector3D) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl ops::Sub<&Vector3D> for &Vector3D {
    type Output = Vector3D;

    fn sub(self, rhs: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z
        }
    }
}

impl ops::SubAssign<&Vector3D> for Vector3D {
    fn sub_assign(&mut self, rhs: &Vector3D) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl ops::Mul<f32> for &Vector3D {
    type Output = Vector3D;

    fn mul(self, rhs: f32) -> Vector3D {
        Vector3D {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs
        }
    }
}

impl ops::MulAssign<f32> for Vector3D {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
        self.z *= rhs;
    }
}

impl ops::Mul for &Vector3D {
    type Output = f32;

    fn mul(self, rhs: &Vector3D) -> f32 {
        self.x * rhs.x + self.y * rhs.y + self.z * rhs.z
    }
}

#[derive(Debug, Clone)]
pub struct Matrix3D {
    pub m11: f32,
    pub m12: f32,
    pub m13: f32,
    pub m14: f32,

    pub m21: f32,
    pub m22: f32,
    pub m23: f32,
    pub m24: f32,

    pub m31: f32,
    pub m32: f32,
    pub m33: f32,
    pub m34: f32,

    pub m41: f32,
    pub m42: f32,
    pub m43: f32,
    pub m44: f32
}

impl Matrix3D {
    pub fn new(m11: f32, m12: f32, m13: f32, m14: f32,
               m21: f32, m22: f32, m23: f32, m24: f32,
               m31: f32, m32: f32, m33: f32, m34: f32,
               m41: f32, m42: f32, m43: f32, m44: f32) -> Matrix3D
    {
        Matrix3D {
            m11, m12, m13, m14,
            m21, m22, m23, m24,
            m31, m32, m33, m34,
            m41, m42, m43, m44
        }
    }

    pub fn from_row_vectors_3x3(v1: &Vector3D, v2: &Vector3D, v3: &Vector3D) -> Matrix3D {
        Matrix3D::new(
            v1.x, v1.y, v1.z, 0.0,
            v2.x, v2.y, v2.z, 0.0,
            v3.x, v3.y, v3.z, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn identity() -> Matrix3D {
        Matrix3D::new(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn transpose(&self) -> Matrix3D {
        Matrix3D::new(
            self.m11, self.m21, self.m31, self.m41,
            self.m12, self.m22, self.m32, self.m42,
            self.m13, self.m23, self.m33, self.m43,
            self.m14, self.m24, self.m34, self.m44
        )
    }

    pub fn determinant(&self) -> f32 {
        let sub3142 = self.m31 * self.m42 - self.m32 * self.m41;
        let sub3143 = self.m31 * self.m43 - self.m33 * self.m41;
        let sub3144 = self.m31 * self.m44 - self.m34 * self.m41;
        let sub3243 = self.m32 * self.m43 - self.m33 * self.m42;
        let sub3244 = self.m32 * self.m44 - self.m34 * self.m42;
        let sub3344 = self.m33 * self.m44 - self.m34 * self.m43;

        self.m11 * (self.m22 * sub3344 - self.m23 * sub3244 + self.m24 * sub3243) -
            self.m12 * (self.m21 * sub3344 - self.m23 * sub3144 + self.m24 * sub3143) +
            self.m13 * (self.m21 * sub3244 - self.m22 * sub3144 + self.m24 * sub3142) -
            self.m14 * (self.m21 * sub3243 - self.m22 * sub3143 + self.m23 * sub3142)
    }

    pub fn invert(&self) -> Matrix3D {
        let sub1122 = self.m11 * self.m22 - self.m12 * self.m21;
        let sub1123 = self.m11 * self.m23 - self.m13 * self.m21;
        let sub1124 = self.m11 * self.m24 - self.m14 * self.m21;
        let sub1223 = self.m12 * self.m23 - self.m13 * self.m22;
        let sub1224 = self.m12 * self.m24 - self.m14 * self.m22;
        let sub1324 = self.m13 * self.m24 - self.m14 * self.m23;

        let sub3142 = self.m31 * self.m42 - self.m32 * self.m41;
        let sub3143 = self.m31 * self.m43 - self.m33 * self.m41;
        let sub3144 = self.m31 * self.m44 - self.m34 * self.m41;
        let sub3243 = self.m32 * self.m43 - self.m33 * self.m42;
        let sub3244 = self.m32 * self.m44 - self.m34 * self.m42;
        let sub3344 = self.m33 * self.m44 - self.m34 * self.m43;

        let det = self.m11 * (self.m22 * sub3344 - self.m23 * sub3244 + self.m24 * sub3243) -
            self.m12 * (self.m21 * sub3344 - self.m23 * sub3144 + self.m24 * sub3143) +
            self.m13 * (self.m21 * sub3244 - self.m22 * sub3144 + self.m24 * sub3142) -
            self.m14 * (self.m21 * sub3243 - self.m22 * sub3143 + self.m23 * sub3142);
        let inv_det = 1.0 / det;

        let c11 = self.m22 * sub3344 - self.m23 * sub3244 + self.m24 * sub3243;
        let c12 = -(self.m21 * sub3344 - self.m23 * sub3144 + self.m24 * sub3143);
        let c13 = self.m21 * sub3244 - self.m22 * sub3144 + self.m24 * sub3142;
        let c14 = -(self.m21 * sub3243 - self.m22 * sub3143 + self.m23 * sub3142);

        let c21 = -(self.m12 * sub3344 - self.m13 * sub3244 + self.m14 * sub3243);
        let c22 = self.m11 * sub3344 - self.m13 * sub3144 + self.m14 * sub3143;
        let c23 = -(self.m11 * sub3244 - self.m12 * sub3144 + self.m14 * sub3142);
        let c24 = self.m11 * sub3243 - self.m12 * sub3143 + self.m13 * sub3142;

        let c31 = self.m42 * sub1324 - self.m43 * sub1224 + self.m44 * sub1223;
        let c32 = -(self.m41 * sub1324 - self.m43 * sub1124 + self.m44 * sub1123);
        let c33 = self.m41 * sub1224 - self.m42 * sub1124 + self.m44 * sub1122;
        let c34 = -(self.m41 * sub1223 - self.m42 * sub1123 + self.m43 * sub1122);

        let c41 = -(self.m32 * sub1324 - self.m33 * sub1224 + self.m34 * sub1223);
        let c42 = self.m31 * sub1324 - self.m33 * sub1124 + self.m34 * sub1123;
        let c43 = -(self.m31 * sub1224 - self.m32 * sub1124 + self.m34 * sub1122);
        let c44 = self.m31 * sub1223 - self.m32 * sub1123 + self.m33 * sub1122;

        Matrix3D::new(
            inv_det * c11, inv_det * c21, inv_det * c31, inv_det * c41,
            inv_det * c12, inv_det * c22, inv_det * c32, inv_det * c42,
            inv_det * c13, inv_det * c23, inv_det * c33, inv_det * c43,
            inv_det * c14, inv_det * c24, inv_det * c34, inv_det * c44
        )
    }

    pub fn translation(d_pos: &Vector3D) -> Matrix3D {
        Matrix3D::new(
            1.0, 0.0, 0.0, d_pos.x,
            0.0, 1.0, 0.0, d_pos.y,
            0.0, 0.0, 1.0, d_pos.z,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn rotation_x(angle: f32) -> Matrix3D {
        let cos_a = angle.cos();
        let sin_a = angle.sin();

        Matrix3D::new(
            1.0, 0.0, 0.0, 0.0,
            0.0, cos_a, -sin_a, 0.0,
            0.0, sin_a, cos_a, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn rotation_y(angle: f32) -> Matrix3D {
        let cos_a = angle.cos();
        let sin_a = angle.sin();

        Matrix3D::new(
            cos_a, 0.0, sin_a, 0.0,
            0.0, 1.0, 0.0, 0.0,
            -sin_a, 0.0, cos_a, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn rotation_z(angle: f32) -> Matrix3D {
        let cos_a = angle.cos();
        let sin_a = angle.sin();

        Matrix3D::new(
            cos_a, -sin_a, 0.0, 0.0,
            sin_a, cos_a, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn rotation_axis(axis: &Vector3D, angle: f32) -> Matrix3D {
        let cos_a = angle.cos();
        let sin_a = angle.sin();
        let one_m_cos_a = 1.0 - cos_a;

        let x_2 = axis.x * axis.x;
        let y_2 = axis.y * axis.y;
        let z_2 = axis.z * axis.z;

        let xy = axis.x * axis.y;
        let xz = axis.x * axis.z;
        let yz = axis.y * axis.z;

        let sx = sin_a * axis.x;
        let sy = sin_a * axis.y;
        let sz = sin_a * axis.z;

        let m11 = one_m_cos_a * x_2 + cos_a;
        let m12 = one_m_cos_a * xy - sz;
        let m13 = one_m_cos_a * xz + sy;

        let m21 = one_m_cos_a * xy + sz;
        let m22 = one_m_cos_a * y_2 + cos_a;
        let m23 = one_m_cos_a * yz - sx;

        let m31 = one_m_cos_a * xz - sy;
        let m32 = one_m_cos_a * yz + sx;
        let m33 = one_m_cos_a * z_2 + cos_a;

        Matrix3D::new(
            m11, m12, m13, 0.0,
            m21, m22, m23, 0.0,
            m31, m32, m33, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn scaling_uniform(s: f32) -> Matrix3D {
        Matrix3D::new(
            s, 0.0, 0.0, 0.0,
            0.0, s, 0.0, 0.0,
            0.0, 0.0, s, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn scaling(sx: f32, sy: f32, sz: f32) -> Matrix3D {
        Matrix3D::new(
            sx, 0.0, 0.0, 0.0,
            0.0, sy, 0.0, 0.0,
            0.0, 0.0, sz, 0.0,
            0.0, 0.0, 0.0, 1.0
        )
    }

    pub fn look_at_dir(pos: &Point3D, dir: &Vector3D, up: &Vector3D) -> error::Result<Matrix3D> {
        if dir.length_sq() < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("dir")),
                "Direction vector has zero length."
            ));
        }

        if up.length_sq() < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("up")),
                "Up vector has zero length."
            ));
        }

        let view_dir = dir.normalize();

        let mut view_up = up - &(&view_dir * (&view_dir * up));
        view_up.normalize_this();

        let view_side = Vector3D::cross(&view_dir, &view_up);

        let mut rot_mtx = Matrix3D::from_row_vectors_3x3(&view_side, &view_up, &(-&view_dir));

        let eye_inv = -(&(&rot_mtx * pos));
        rot_mtx.m14 = eye_inv.x;
        rot_mtx.m24 = eye_inv.y;
        rot_mtx.m34 = eye_inv.z;

        Ok(rot_mtx)
    }

    pub fn look_at(pos: &Point3D, target: &Point3D, up: &Vector3D) -> error::Result<Matrix3D> {
        let dir = target - pos;

        Matrix3D::look_at_dir(pos, &dir, up)
    }

    pub fn perspective_gl(fov_y: f32, aspect: f32, z_near: f32, z_far: f32) -> error::Result<Matrix3D> {
        if fov_y < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("fov_y")),
                &format!("Vertical FOV must be greater than 0.\nFOV: {}", fov_y)
            ));
        }

        let nmf = z_near - z_far;
        let npf = z_near + z_far;
        if nmf > -EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("z_near, z_far")),
                &format!(
                    "Far Z value must be greater than near Z value.\nZ near: {}\nZ far: {}",
                    z_near,
                    z_far
                )
            ));
        }
        let rev_nmf = 1.0 / nmf;

        let d = 1.0 / (fov_y * 0.5).tan();
        let two_nf = 2.0 * z_near * z_far;

        Ok(Matrix3D::new(
            d / aspect, 0.0, 0.0, 0.0,
            0.0, d, 0.0, 0.0,
            0.0, 0.0, npf * rev_nmf, two_nf * rev_nmf,
            0.0, 0.0, -1.0, 0.0
        ))
    }

    pub fn ortho_gl(left: f32, right: f32, bottom: f32, top: f32, z_near: f32, z_far: f32) -> error::Result<Matrix3D> {
        let rml = right - left;
        if rml.abs() < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("left, right")),
                &format!(
                    "Left and right values must be different.\nLeft: {}\nRight: {}",
                    left,
                    right
                )
            ));
        }
        let rpl = right + left;
        let rev_rml = 1.0 / rml;

        let tmb = top - bottom;
        if rml.abs() < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("top, bottom")),
                &format!(
                    "Top and bottom values must be different.\nTop: {}\nBottom: {}",
                    top,
                    bottom
                )
            ));
        }
        let tpb = top + bottom;
        let rev_tmb = 1.0 / tmb;

        let fmn = z_far - z_near;
        if fmn < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("z_near, z_far")),
                &format!(
                    "Far Z value must be greater than near Z value.\nZ near: {}\nZ far: {}",
                    z_near,
                    z_far
                )
            ));
        }
        let fpn = z_far + z_near;
        let rev_fmn = 1.0 / fmn;

        Ok(Matrix3D::new(
            2.0 * rev_rml, 0.0, 0.0, -rpl * rev_rml,
            0.0, 2.0 * rev_tmb, 0.0, -tpb * rev_tmb,
            0.0, 0.0, -2.0 * rev_fmn, -fpn * rev_fmn,
            0.0, 0.0, 0.0, 1.0
        ))
    }
}

impl cmp::PartialEq for Matrix3D {
    fn eq(&self, other: &Matrix3D) -> bool {
        (self.m11 == other.m11) && (self.m12 == other.m12) && (self.m13 == other.m13) && (self.m14 == other.m14) &&
            (self.m21 == other.m21) && (self.m22 == other.m22) && (self.m23 == other.m23) && (self.m24 == other.m24) &&
            (self.m31 == other.m31) && (self.m32 == other.m32) && (self.m33 == other.m33) && (self.m34 == other.m34) &&
            (self.m41 == other.m41) && (self.m42 == other.m42) && (self.m43 == other.m43) && (self.m44 == other.m44)
    }
}
impl cmp::Eq for Matrix3D {}

impl ops::Mul for &Matrix3D {
    type Output = Matrix3D;

    fn mul(self, rhs: &Matrix3D) -> Matrix3D {
        let m11 = self.m11 * rhs.m11 + self.m12 * rhs.m21 + self.m13 * rhs.m31 + self.m14 * rhs.m41;
        let m12 = self.m11 * rhs.m12 + self.m12 * rhs.m22 + self.m13 * rhs.m32 + self.m14 * rhs.m42;
        let m13 = self.m11 * rhs.m13 + self.m12 * rhs.m23 + self.m13 * rhs.m33 + self.m14 * rhs.m43;
        let m14 = self.m11 * rhs.m14 + self.m12 * rhs.m24 + self.m13 * rhs.m34 + self.m14 * rhs.m44;

        let m21 = self.m21 * rhs.m11 + self.m22 * rhs.m21 + self.m23 * rhs.m31 + self.m24 * rhs.m41;
        let m22 = self.m21 * rhs.m12 + self.m22 * rhs.m22 + self.m23 * rhs.m32 + self.m24 * rhs.m42;
        let m23 = self.m21 * rhs.m13 + self.m22 * rhs.m23 + self.m23 * rhs.m33 + self.m24 * rhs.m43;
        let m24 = self.m21 * rhs.m14 + self.m22 * rhs.m24 + self.m23 * rhs.m34 + self.m24 * rhs.m44;

        let m31 = self.m31 * rhs.m11 + self.m32 * rhs.m21 + self.m33 * rhs.m31 + self.m34 * rhs.m41;
        let m32 = self.m31 * rhs.m12 + self.m32 * rhs.m22 + self.m33 * rhs.m32 + self.m34 * rhs.m42;
        let m33 = self.m31 * rhs.m13 + self.m32 * rhs.m23 + self.m33 * rhs.m33 + self.m34 * rhs.m43;
        let m34 = self.m31 * rhs.m14 + self.m32 * rhs.m24 + self.m33 * rhs.m34 + self.m34 * rhs.m44;

        let m41 = self.m41 * rhs.m11 + self.m42 * rhs.m21 + self.m43 * rhs.m31 + self.m44 * rhs.m41;
        let m42 = self.m41 * rhs.m12 + self.m42 * rhs.m22 + self.m43 * rhs.m32 + self.m44 * rhs.m42;
        let m43 = self.m41 * rhs.m13 + self.m42 * rhs.m23 + self.m43 * rhs.m33 + self.m44 * rhs.m43;
        let m44 = self.m41 * rhs.m14 + self.m42 * rhs.m24 + self.m43 * rhs.m34 + self.m44 * rhs.m44;

        Matrix3D {
            m11, m12, m13, m14,
            m21, m22, m23, m24,
            m31, m32, m33, m34,
            m41, m42, m43, m44
        }
    }
}

impl ops::MulAssign<&Matrix3D> for Matrix3D {
    fn mul_assign(&mut self, rhs: &Matrix3D) {
        let m11 = self.m11 * rhs.m11 + self.m12 * rhs.m21 + self.m13 * rhs.m31 + self.m14 * rhs.m41;
        let m12 = self.m11 * rhs.m12 + self.m12 * rhs.m22 + self.m13 * rhs.m32 + self.m14 * rhs.m42;
        let m13 = self.m11 * rhs.m13 + self.m12 * rhs.m23 + self.m13 * rhs.m33 + self.m14 * rhs.m43;
        let m14 = self.m11 * rhs.m14 + self.m12 * rhs.m24 + self.m13 * rhs.m34 + self.m14 * rhs.m44;

        let m21 = self.m21 * rhs.m11 + self.m22 * rhs.m21 + self.m23 * rhs.m31 + self.m24 * rhs.m41;
        let m22 = self.m21 * rhs.m12 + self.m22 * rhs.m22 + self.m23 * rhs.m32 + self.m24 * rhs.m42;
        let m23 = self.m21 * rhs.m13 + self.m22 * rhs.m23 + self.m23 * rhs.m33 + self.m24 * rhs.m43;
        let m24 = self.m21 * rhs.m14 + self.m22 * rhs.m24 + self.m23 * rhs.m34 + self.m24 * rhs.m44;

        let m31 = self.m31 * rhs.m11 + self.m32 * rhs.m21 + self.m33 * rhs.m31 + self.m34 * rhs.m41;
        let m32 = self.m31 * rhs.m12 + self.m32 * rhs.m22 + self.m33 * rhs.m32 + self.m34 * rhs.m42;
        let m33 = self.m31 * rhs.m13 + self.m32 * rhs.m23 + self.m33 * rhs.m33 + self.m34 * rhs.m43;
        let m34 = self.m31 * rhs.m14 + self.m32 * rhs.m24 + self.m33 * rhs.m34 + self.m34 * rhs.m44;

        let m41 = self.m41 * rhs.m11 + self.m42 * rhs.m21 + self.m43 * rhs.m31 + self.m44 * rhs.m41;
        let m42 = self.m41 * rhs.m12 + self.m42 * rhs.m22 + self.m43 * rhs.m32 + self.m44 * rhs.m42;
        let m43 = self.m41 * rhs.m13 + self.m42 * rhs.m23 + self.m43 * rhs.m33 + self.m44 * rhs.m43;
        let m44 = self.m41 * rhs.m14 + self.m42 * rhs.m24 + self.m43 * rhs.m34 + self.m44 * rhs.m44;

        self.m11 = m11;
        self.m12 = m12;
        self.m13 = m13;
        self.m14 = m14;

        self.m21 = m21;
        self.m22 = m22;
        self.m23 = m23;
        self.m24 = m24;

        self.m31 = m31;
        self.m32 = m32;
        self.m33 = m33;
        self.m34 = m34;

        self.m41 = m41;
        self.m42 = m42;
        self.m43 = m43;
        self.m44 = m44;
    }
}

impl ops::Mul<&Point3D> for &Matrix3D {
    type Output = Point3D;

    fn mul(self, rhs: &Point3D) -> Point3D {
        let mut x = self.m11 * rhs.x + self.m12 * rhs.y + self.m13 * rhs.z + self.m14;
        let mut y = self.m21 * rhs.x + self.m22 * rhs.y + self.m23 * rhs.z + self.m24;
        let mut z = self.m31 * rhs.x + self.m32 * rhs.y + self.m33 * rhs.z + self.m34;

        let w = self.m41 * rhs.x + self.m42 * rhs.y + self.m43 * rhs.z + self.m44;
        if w.abs() > EPSILON {
            let rev_w = 1.0 / w;

            x *= rev_w;
            y *= rev_w;
            z *= rev_w;
        }

        Point3D {
            x,
            y,
            z
        }
    }
}

impl ops::Mul<&Vector3D> for &Matrix3D {
    type Output = Vector3D;

    fn mul(self, rhs: &Vector3D) -> Vector3D {
        let mut x = self.m11 * rhs.x + self.m12 * rhs.y + self.m13 * rhs.z;
        let mut y = self.m21 * rhs.x + self.m22 * rhs.y + self.m23 * rhs.z;
        let mut z = self.m31 * rhs.x + self.m32 * rhs.y + self.m33 * rhs.z;

        Vector3D {
            x,
            y,
            z
        }
    }
}
