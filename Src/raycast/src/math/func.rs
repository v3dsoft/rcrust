use std::f32::consts::PI;

pub const EPSILON: f32 = 0.0001;

pub const fn linear_coord(x: u32, y: u32, width: u32) -> u32 {
    x + y * width
}

pub fn fov_v_angle_from_h(h_angle: f32, aspect: f32) -> f32 {
    if aspect < EPSILON {
        return 0.0
    }

    let h_2 = h_angle * 0.5;
    let v_2 = (h_2.tan() / aspect).atan();

    v_2 * 2.0
}

pub fn fov_h_angle_from_v(v_angle: f32, aspect: f32) -> f32 {
    let v_2 = v_angle * 0.5;
    let h_2 = (v_2.tan() * aspect).atan();

    h_2 * 2.0
}

pub fn normalize_angle(angle: f32) -> f32 {
    const ANGLE_360: f32 = PI * 2.0;

    let mut norm_angle = angle;

    while norm_angle > ANGLE_360 {
        norm_angle -= ANGLE_360;
    }

    while norm_angle < -ANGLE_360 {
        norm_angle += ANGLE_360;
    }

    norm_angle
}

pub trait F32Funcs {
    fn approx_eq(self, rhs: f32) -> bool;
    fn approx_greater(self, rhs: f32) -> bool;
    fn approx_ge(self, rhs: f32) -> bool;
    fn approx_less(self, rhs: f32) -> bool;
    fn approx_le(self, rhs: f32) -> bool;

    fn rsqrt(self) -> f32;
}

impl F32Funcs for f32 {
    fn approx_eq(self, rhs: f32) -> bool { (self - rhs).abs() < EPSILON }
    fn approx_greater(self, rhs: f32) -> bool { self > rhs - EPSILON }
    fn approx_ge(self, rhs: f32) -> bool { self >= rhs - EPSILON }
    fn approx_less(self, rhs: f32) -> bool { self < rhs + EPSILON }
    fn approx_le(self, rhs: f32) -> bool { self < rhs + EPSILON }

    fn rsqrt(self) -> f32 {
        #[cfg(target_arch = "x86_64")]
        use std::arch::x86_64::{
            _mm_load_ss,
            _mm_store_ss,
            _mm_sqrt_ss
        };

        #[cfg(target_arch = "x86")]
        use std::arch::x86::{
            _mm_load_ss,
            _mm_store_ss,
            _mm_sqrt_ss
        };

        unsafe {
            let val = _mm_load_ss(&self);
            let res = _mm_sqrt_ss(val);

            let mut fres: f32 = 0.0;
            _mm_store_ss(&mut fres, res);

            fres
        }
    }
}