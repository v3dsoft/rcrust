#[derive(Debug, Clone, PartialEq)]
pub struct PixelSize {
    pub width: u32,
    pub height: u32
}
