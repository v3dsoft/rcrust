use super::F32Funcs;

pub type XRGB = u32;
pub type ARGB = u32;

pub fn xrgb_from_components(r: u8, g: u8, b: u8) -> XRGB {
    ((r as u32) << 16) + ((g as u32) << 8) + (b as u32)
}

pub fn argb_from_components(a: u8, r: u8, g: u8, b: u8) -> ARGB {
    ((a as u32) << 24) + ((r as u32) << 16) + ((g as u32) << 8) + (b as u32)
}

#[derive(Clone)]
pub struct Color {
    pub a: f32,
    pub r: f32,
    pub g: f32,
    pub b: f32
}

impl Color {
    pub const fn new_xrgb(r: f32, g: f32, b: f32) -> Color {
        Color {a: 1.0, r, g, b}
    }

    pub const fn new_argb(a: f32, r: f32, g: f32, b: f32) -> Color {
        Color {a, r, g, b}
    }

    pub fn to_xrgb(&self) -> XRGB {
        let mut xrgb = 0x0000ff00 as XRGB;

        let r = (self.r * 255.0f32) as XRGB;
        xrgb += r;

        let g = (self.g * 255.0f32) as XRGB;
        xrgb <<= 8;
        xrgb += g;

        let b = (self.b * 255.0f32) as XRGB;
        xrgb <<= 8;
        xrgb += b;

        xrgb
    }

    pub fn to_argb(&self) -> ARGB {
        let mut argb = 0 as ARGB;

        let a = (self.a * 255.0f32) as ARGB;
        argb += a;

        let r = (self.r * 255.0f32) as ARGB;
        argb <<= 8;
        argb += r;

        let g = (self.g * 255.0f32) as ARGB;
        argb <<= 8;
        argb += g;

        let b = (self.b * 255.0f32) as ARGB;
        argb <<= 8;
        argb += b;

        argb
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Color) -> bool {
        self.a.approx_eq(other.a) &&
            self.r.approx_eq(other.r) &&
            self.g.approx_eq(other.g) &&
            self.b.approx_eq(other.b)
    }
}

impl Eq for Color {}