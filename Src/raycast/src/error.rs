use std::fmt;
use std::result;

#[derive(Clone)]
pub enum Cause {
    NotImplemented,
    Internal,
    WrongArgument(String),
    WrongSetup,
    WrongFormat,
    FileError
}

pub struct Error {
    pub cause: Cause,
    pub message: String
}

pub type Result<T> = result::Result<T, Error>;

impl Error {
    pub fn new(cause: Cause, message: &str) -> Error {
        Error {
            cause,
            message: String::from(message)
        }
    }

    pub fn explain(&self) -> String {
        match self.cause {
            Cause::NotImplemented => format!("Not implemented error: {}", self.message),
            Cause::Internal => format!("Internal error: {}", self.message),
            Cause::WrongArgument(ref arg) => format!("Wrong argument ({}): {}", arg, self.message),
            Cause::WrongSetup => format!("Wrong setup: {}", self.message),
            Cause::WrongFormat => format!("Wrong format: {}", self.message),
            Cause::FileError => format!("File error: {}", self.message)
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}", self.explain())
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> result::Result<(), fmt::Error> {
        f.write_str(&self.explain())
    }
}