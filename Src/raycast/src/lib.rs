pub mod error;
pub mod math;

pub mod engine;
pub use crate::engine::{Engine, EngineRC};

pub mod render;

pub mod scene;

pub mod utils;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
