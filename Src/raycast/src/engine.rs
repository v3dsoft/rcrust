use std::rc::Rc;
use std::cell::RefCell;

use crate::error;

use crate::render::driver::RenderDriver;
use crate::render::driver::gl33::RenderDriverGL33;

use crate::utils::sdl::SdlInstance;
use sdl2;

use crate::scene;

#[derive(Clone)]
pub struct Resolution {
    pub width: u32,
    pub height: u32
}
pub type ResolutionList = Vec<Resolution>;

#[derive(Clone)]
pub struct RenderParams {
    pub title: String,
    pub resolution: Resolution,
    pub fullscreen: bool,
    pub vsync: bool
}

pub struct InitParams {
    render: RenderParams
}

pub struct Engine {
    rd: Box<dyn RenderDriver>,

    sdl: SdlInstance,
    event_pump: sdl2::EventPump
}

pub type EngineRC = Rc<RefCell<Engine>>;

impl Engine {
    pub fn new(params: &InitParams) -> error::Result<Engine> {
        let rd = Box::new(RenderDriverGL33::new(&params.render)?);

        let sdl = SdlInstance::new()?;
        let event_pump = sdl.init_event_pump()?;

        Ok(Engine {
            rd,

            sdl,
            event_pump
        })
    }

    pub fn build() -> EngineBuilder {
        EngineBuilder::new()
    }

    pub fn update(&mut self) -> bool {
        use sdl2::event::Event;
        use sdl2::keyboard::Keycode;    //TODO: Extract this to proper input manager

        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit{..} | Event::KeyDown {keycode: Some(Keycode::Escape), ..} => {
                    return false;
                },
                _ => {}
            }
        }

        true
    }

    pub fn present(&mut self) {
        self.rd.present();
    }

    pub fn build_scene(engine: EngineRC, width: u32, height: u32) -> error::Result<scene::SceneBuilder> {
        scene::SceneBuilder::new(engine, width, height)
    }

    pub fn render_driver(&self) -> &dyn RenderDriver {
        self.rd.as_ref()
    }

    pub fn render_driver_mut(&mut self) -> &mut dyn RenderDriver {
        self.rd.as_mut()
    }
}

pub struct EngineBuilder {
    params: InitParams
}

impl EngineBuilder {
    fn new() -> Self {
        EngineBuilder {
            params: InitParams {
                render: RenderParams {
                    title: String::from("RayCast Rust"),
                    resolution: Resolution {
                        width: 640,
                        height: 480
                    },
                    fullscreen: false,
                    vsync: false
                }
            }
        }
    }

    pub fn render_title(mut self, title: &str) -> Self {
        self.params.render.title = String::from(title);
        self
    }

    pub fn render_resolution(mut self, resolution: &Resolution) -> Self {
        self.params.render.resolution.width = resolution.width;
        self.params.render.resolution.height = resolution.height;

        self
    }

    pub fn render_fullscreen(mut self, fullscreen: bool) -> Self {
        self.params.render.fullscreen = fullscreen;
        self
    }

    pub fn render_vsync(mut self, vsync: bool) -> Self {
        self.params.render.vsync = vsync;
        self
    }

    pub fn create(self) -> error::Result<EngineRC> {
        let engine = Engine::new(&self.params)?;
        Ok(Rc::new(RefCell::new(engine)))
    }
}