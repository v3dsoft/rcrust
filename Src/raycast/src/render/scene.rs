use std::cell::RefCell;

mod camera;
pub use self::camera::*;

pub(self) mod view;
pub(self) mod walker;
pub(self) mod data;

pub(self) mod portals;
use self::portals::{
    PortalCell,
    PortalFlags
};

pub(self) mod utils;

mod wall;

mod renderer;
use self::renderer::SceneRenderer;

use crate::render::driver::RenderDriver;

use crate::error;
use crate::math::{
    Point2D,

    Point3D,
    Vector3D,

    linear_coord,

    EPSILON,
    F32Funcs
};

#[derive(Copy, Clone)]
pub enum WallDir {
    N,
    S,
    E,
    W
}

impl WallDir {
    pub fn reversed(self) -> WallDir {
        match self {
            WallDir::N => WallDir::S,
            WallDir::E => WallDir::W,
            WallDir::S => WallDir::N,
            WallDir::W => WallDir::E
        }
    }
}

pub struct RenderPlane {
    pub height: f32
}

pub enum CellFill {
    Wall,
    Space {
        floor: RenderPlane,
        ceiling: RenderPlane
    }
}

struct RenderCell {
    pub walls: [Option<u32>; 4],
    pub fill: CellFill
}

impl RenderCell {
    fn new() -> RenderCell {
        RenderCell {
            walls: [None; 4],
            fill: CellFill::Wall
        }
    }
}

pub enum PointPos2D {
    Inside(u32, u32),
    Outside
}

pub(self) struct CellInfo<'a> {
    scene: &'a RenderScene,
    index: usize,
    origin: Point2D
}

impl<'a> CellInfo<'a> {
    pub fn origin(&self) -> &Point2D {
        &self.origin
    }

    pub fn fill(&self) -> &CellFill {
        &self.scene.data[self.index].fill
    }

    pub fn wall(&self, dir: WallDir) -> Option<(usize, &wall::Wall)> {
        let wall_index = dir as usize;
        match self.scene.data[self.index].walls[wall_index] {
            Some(wi) => {
                let wi = wi as usize;
                Some((wi, &self.scene.wall_data[wi].0))
            },
            None => None
        }
    }
}

pub struct RenderScene {
    width: u32,
    height: u32,

    cell_size: f32,

    data: Vec<RenderCell>,
    wall_data: Vec<(wall::Wall, bool)>,

    inside_update: bool,

    renderer: RefCell<SceneRenderer>,
}

impl RenderScene {
    pub fn new(width: u32, height: u32, cell_size: f32) -> error::Result<RenderScene> {
        if (width == 0) || (height == 0) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("width, height")),
                &format!("Render scene size must be greater than 0.\nSize: {}x{}", width, height)
            ));
        }

        if cell_size < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("cell_size")),
                &format!("Cell size must be greater than 0.\nSize: {}", cell_size)
            ));
        }

        let data_size = (width * height) as usize;
        let mut data = Vec::with_capacity(data_size);
        for _cell in 0..data_size {
            data.push(RenderCell::new());
        }

        let wall_data = Vec::new();

        let renderer = RefCell::new(SceneRenderer::new()?);

        Ok(RenderScene{
            width,
            height,

            cell_size,

            data,
            wall_data,

            inside_update: false,

            renderer,
        })
    }

    pub fn create_camera(&self, pos: &Point3D, h_angle: f32, v_angle: f32, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        Camera::new(pos, h_angle, v_angle, fov_v_angle, aspect)
    }

    pub fn create_camera_dir(&self, pos: &Point3D, dir: &Vector3D, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        Camera::with_dir(pos, dir, fov_v_angle, aspect)
    }

    pub fn create_camera_target(&self, pos: &Point3D, tgt: &Point3D, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        Camera::with_target(pos, tgt, fov_v_angle, aspect)
    }

    pub fn begin_update(&mut self) -> RSUpdater {
        self.inside_update = true;

        RSUpdater {
            scene: self
        }
    }

    pub fn set_cell(&mut self, x: u32, y: u32, cell: CellFill) -> error::Result<()> {
        if !self.check_cell(x, y) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("x, y")),
                "Invalid cell coordinates."
            ));
        }

        let cell_id = linear_coord(x, y, self.width);
        self.data[cell_id as usize].fill = cell;

        if !self.inside_update {
            self.update_walls_around(cell_id);
        }

        Ok(())
    }

    fn remove_cell_wall(&mut self, cell_id: usize, dir: WallDir) {
        match self.data[cell_id].walls[dir as usize] {
            Some(wall_id) => {
                self.wall_data[wall_id as usize].1 = true;
                self.data[cell_id].walls[dir as usize] = None;
            },

            None => {}  //It's already empty
        }
    }

    fn remove_cell_common_wall(&mut self, cell_id: usize, other_id: usize, dir: WallDir) {
        let dir_id = dir as usize;
        let other_dir_id = dir.reversed() as usize;

        match self.data[cell_id].walls[dir_id] {
            Some(wall_id) => {
                self.data[cell_id].walls[dir_id] = None;
                self.data[other_id].walls[other_dir_id] = None;

                self.wall_data[wall_id as usize].1 = true;
            },

            None => {}  //It's already empty
        }
    }

    fn register_wall(&mut self, wall: wall::Wall) -> u32 {
        let mut wall_id: u32 = 0;
        for wall_rec in &mut self.wall_data {
            if wall_rec.1 {
                *wall_rec = (wall, false);
                return wall_id;
            }

            wall_id += 1;
        }

        self.wall_data.push((wall, false));
        wall_id
    }

    fn update_cell_wall(&mut self, cell_id: usize, dir: WallDir, wall: wall::Wall) {
        let dir_id = dir as usize;

        match self.data[cell_id].walls[dir_id] {
            Some(wall_id) => {
                self.wall_data[wall_id as usize] = (wall, false);
            },

            None => {
                let wall_id = self.register_wall(wall);
                self.data[cell_id].walls[dir_id] = Some(wall_id);
            }
        }
    }

    fn update_cell_common_wall(&mut self, cell_id: usize, other_id: usize, dir: WallDir, wall: wall::Wall) {
        let dir_id = dir as usize;

        match self.data[cell_id].walls[dir_id] {
            Some(wall_id) => {
                self.wall_data[wall_id as usize] = (wall, false);
            },

            None => {
                let other_dir_id = dir.reversed() as usize;
                let wall_id = self.register_wall(wall);

                self.data[cell_id].walls[dir_id] = Some(wall_id);
                self.data[other_id].walls[other_dir_id] = Some(wall_id);
            }
        }
    }

    fn calc_wall_origin(&self, cell_x: f32, cell_y: f32, floor_z: f32, dir: WallDir) -> Point3D {
        match dir {
            WallDir::N => Point3D {
                x: cell_x,
                y: cell_y + self.cell_size,
                z: floor_z
            },

            WallDir::E => Point3D {
                x: cell_x + self.cell_size,
                y: cell_y,
                z: floor_z
            },

            WallDir::S | WallDir::W => Point3D {
                x: cell_x,
                y: cell_y,
                z: floor_z
            }
        }
    }

    fn update_separate_wall(&mut self, cell_id: usize, cell_x: f32, cell_y: f32, dir: WallDir) {
        match self.data[cell_id].fill {
            CellFill::Wall => {
                self.remove_cell_wall(cell_id, dir);
            },

            CellFill::Space { ref floor, ref ceiling } => {
                let wall_origin = self.calc_wall_origin(cell_x, cell_y, floor.height, dir);
                let wall_height = ceiling.height - floor.height;

                let wall = wall::Wall::simple(&wall_origin, dir, wall_height);
                self.update_cell_wall(cell_id, dir, wall);
            }
        }
    }

    fn neighbour_cell_id(&self, cell_id: u32, dir: WallDir) -> Option<u32> {
        match dir {
            WallDir::N => {
                if cell_id > self.width {
                    Some(cell_id - self.width)
                } else {
                    None
                }
            },

            WallDir::E => {
                if (cell_id % self.width) < (self.width - 1) {
                    Some(cell_id + 1)
                } else {
                    None
                }
            },

            WallDir::S => {
                if cell_id < self.data.len() as u32 - self.width {
                    Some(cell_id + self.width)
                } else {
                    None
                }
            },

            WallDir::W => {
                if cell_id % self.width > 0 {
                    Some(cell_id - 1)
                } else {
                    None
                }
            }
        }
    }

    fn neighbour_cell_start(&self, cell_x: f32, cell_y: f32, dir: WallDir) -> (f32, f32) {
        match dir {
            WallDir::N => (cell_x, cell_y + self.cell_size),
            WallDir::E => (cell_x + self.cell_size, cell_y),
            WallDir::S => (cell_x, cell_y - self.cell_size),
            WallDir::W => (cell_x - self.cell_size, cell_y)
        }
    }

    fn update_common_wall(&mut self, cell_id: usize, cell_x: f32, cell_y: f32, dir: WallDir) {
        let other_id = self.neighbour_cell_id(cell_id as u32, dir).expect(&format!("Cell must have a neighbour.\nCell ID: {}", cell_id)) as usize;

        match &self.data[cell_id].fill {
            CellFill::Space { floor, ceiling } => {
                match &self.data[other_id].fill {
                    CellFill::Space { floor: other_floor, ceiling: other_ceiling } => {
                        //Check simple cases fist
                        if floor.height.approx_ge(other_ceiling.height) {
                            let wall_origin = self.calc_wall_origin(cell_x, cell_y, other_floor.height, dir);

                            let mut wall = wall::Wall::build(&wall_origin, dir);
                            wall.add_segment_top(other_ceiling.height, true);
                            if !floor.height.approx_eq(other_ceiling.height) {
                                wall.add_space_top(floor.height);
                            }
                            wall.add_segment_top(ceiling.height, false);

                            let wall = wall.create();
                            self.update_cell_common_wall(cell_id, other_id, dir, wall);
                        } else if ceiling.height.approx_le(other_floor.height) {
                            let wall_origin = self.calc_wall_origin(cell_x, cell_y, floor.height, dir);

                            let mut wall = wall::Wall::build(&wall_origin, dir);
                            wall.add_segment_top(ceiling.height, false);
                            if !other_floor.height.approx_eq(ceiling.height) {
                                wall.add_space_top(other_floor.height);
                            }
                            wall.add_segment_top(other_ceiling.height, true);

                            let wall = wall.create();
                            self.update_cell_common_wall(cell_id, other_id, dir, wall);
                        } else if floor.height.approx_eq(other_floor.height) && ceiling.height.approx_eq(other_ceiling.height) {
                            self.remove_cell_common_wall(cell_id, other_id, dir);
                        } else {
                            //When building bottom segment approx comparison is required only in equal case
                            let mut wall = if floor.height.approx_eq(other_floor.height) {
                                let wall_origin = self.calc_wall_origin(cell_x, cell_y, floor.height, dir);
                                wall::Wall::build(&wall_origin, dir)
                            } else if floor.height < other_floor.height {
                                let wall_origin = self.calc_wall_origin(cell_x, cell_y, floor.height, dir);

                                let mut wall = wall::Wall::build(&wall_origin, dir);
                                wall.add_segment_top(other_floor.height, false);

                                wall
                            } else {
                                let wall_origin = self.calc_wall_origin(cell_x, cell_y, other_floor.height, dir);

                                let mut wall = wall::Wall::build(&wall_origin, dir);
                                wall.add_segment_top(floor.height, true);

                                wall
                            };

                            //Same here, only equality comparison needs to be approximate
                            if !ceiling.height.approx_eq(other_ceiling.height) {
                                if ceiling.height < other_ceiling.height {
                                    wall.add_space_top(ceiling.height);
                                    wall.add_segment_top(other_ceiling.height, true);
                                } else {
                                    wall.add_space_top(other_ceiling.height);
                                    wall.add_segment_top(ceiling.height, false);
                                }
                            }

                            let wall = wall.create();
                            self.update_cell_common_wall(cell_id, other_id, dir, wall);
                        }
                    },

                    CellFill::Wall => {
                        let wall_height = ceiling.height - floor.height;
                        let wall_origin = self.calc_wall_origin(cell_x, cell_y, floor.height, dir);

                        let wall = wall::Wall::simple(&wall_origin, dir, wall_height);
                        self.update_cell_common_wall(cell_id, other_id, dir, wall);
                    }
                }
            },

            CellFill::Wall => {
                match &self.data[other_id].fill {
                    CellFill::Space { floor: other_floor, ceiling: other_ceiling } => {
                        let (other_x, other_y) = self.neighbour_cell_start(cell_x, cell_y, dir);
                        let rev_dir = dir.reversed();

                        let wall_height = other_ceiling.height - other_floor.height;
                        let wall_origin = self.calc_wall_origin(other_x, other_y, other_floor.height, rev_dir);

                        let wall = wall::Wall::simple(&wall_origin, rev_dir, wall_height);
                        self.update_cell_common_wall(cell_id, other_id, dir, wall);
                    },

                    CellFill::Wall => {
                        self.remove_cell_common_wall(cell_id, other_id, dir);
                    }
                }
            }
        }
    }

    fn update_walls(&mut self) {
        struct Crawler {
            pub x: u32,
            pub y: u32,
            pub id: usize,

            width: u32,
            height: u32,

            pub common_w: bool,
            pub common_n: bool,
            pub common_e: bool,
            pub common_s: bool,

            cell_size: f32,
            pub cell_x: f32,
            pub cell_y: f32
        }

        impl Crawler {
            pub fn new(width: u32, height: u32, cell_size: f32) -> Crawler {
                Crawler {
                    x: 0,
                    y: 0,
                    id: 0,

                    width,
                    height,

                    common_w: false,
                    common_n: false,
                    common_e: width > 1,
                    common_s: height > 1,

                    cell_size,
                    cell_x: 0.0,
                    cell_y: ((height - 1) as f32) * cell_size
                }
            }

            pub fn next_cell(&mut self) -> bool {
                self.x += 1;
                self.id += 1;

                if self.x < self.width {
                    self.common_w = true;
                    self.common_e = self.x < (self.width - 1);

                    self.cell_x += self.cell_size;

                    true
                } else {
                    self.y += 1;
                    if self.y >= self.height {
                        return false;
                    }

                    self.x = 0;

                    self.common_w = false;
                    self.common_n = true;
                    self.common_e = self.width > 1;
                    self.common_s = self.y < (self.height - 1);

                    self.cell_x = 0.0;
                    self.cell_y -= self.cell_size;

                    true
                }
            }
        }

        let mut crawler = Crawler::new(self.width, self.height, self.cell_size);
        loop {
            //West wall
            if !crawler.common_w {
                self.update_separate_wall(crawler.id, crawler.cell_x, crawler.cell_y, WallDir::W);
            }

            //North wall
            if !crawler.common_n {
                self.update_separate_wall(crawler.id, crawler.cell_x, crawler.cell_y, WallDir::N);
            }

            //East wall
            if crawler.common_e {
                self.update_common_wall(crawler.id, crawler.cell_x, crawler.cell_y, WallDir::E);
            } else {
                self.update_separate_wall(crawler.id, crawler.cell_x, crawler.cell_y, WallDir::E);
            }

            //South wall
            if crawler.common_s {
                self.update_common_wall(crawler.id, crawler.cell_x, crawler.cell_y, WallDir::S);
            } else {
                self.update_separate_wall(crawler.id, crawler.cell_x, crawler.cell_y, WallDir::S);
            }

            if !crawler.next_cell() {
                break;
            }
        }
    }

    fn update_walls_around(&mut self, _index: u32) {
        panic!("Not implemented yet.");
    }

    fn end_update(&mut self) {
        self.inside_update = false;
        self.update_walls();
    }

    fn check_cell(&self, x: u32, y: u32) -> bool {
        (x < self.width) && (y < self.width)
    }

    pub fn classify_point(&self, pt: &Point3D) -> PointPos2D {
        let x = (pt.x / self.cell_size).floor() as i32;
        if (x < 0) || (x >= self.width as i32) {
            return PointPos2D::Outside;
        }

        let y = (self.height - 1) as i32 - (pt.y / self.cell_size).floor() as i32;
        if (y < 0) || (y >= self.height as i32) {
            return PointPos2D::Outside;
        }

        PointPos2D::Inside (
            x as u32,
            y as u32
        )
    }

    pub fn cell_origin(&self, x: u32, y: u32) -> Point2D {
        Point2D {
            x: (x as f32) * self.cell_size,
            y: ((self.height - 1 - y) as f32) * self.cell_size
        }
    }

    pub fn cell_is_space(&self, x: u32, y: u32) -> bool {
        if !self.check_cell(x, y) {
            return false;
        }

        let cell_index = linear_coord(x, y, self.width) as usize;

        match self.data[cell_index].fill {
            CellFill::Space {..} => true,
            CellFill::Wall => false
        }
    }

    fn cell_portal_flags(&self, x: u32, y: u32) -> u8 {
        let mut flags: u8 = 0;

        // North
        if (y > 0) && self.cell_is_space(x, y - 1) {
            flags |= PortalFlags::N as u8;
        }

        // East
        if (x < (self.width - 1)) && self.cell_is_space(x + 1, y) {
            flags |= PortalFlags::E as u8;
        }

        // South
        if (y < (self.height - 1)) && self.cell_is_space(x, y + 1) {
            flags |= PortalFlags::S as u8;
        }

        // West
        if (x > 0) && self.cell_is_space(x - 1, y) {
            flags |= PortalFlags::W as u8;
        }

        flags
    }

    pub(self) fn cell_portals(&self, x: u32, y: u32) -> error::Result<PortalCell> {
        if !self.check_cell(x, y) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("x, y")),
                &format!("Cell is out of scene bounds.\nCell: {}, {}", x, y)
            ));
        }

        let origin = self.cell_origin(x, y);
        let portals = self.cell_portal_flags(x, y);

        Ok(PortalCell::new(&origin, self.cell_size, portals)?)

    }

    pub(self) fn cell_info(&self, x: u32, y: u32) -> Option<CellInfo> {
        if !self.check_cell(x, y) {
            return None;
        }

        let index = linear_coord(x, y, self.width) as usize;
        let origin = self.cell_origin(x, y);

        Some(CellInfo {
            scene: self,
            index,
            origin
        })
    }

    pub fn render(&self, driver: &mut dyn RenderDriver, camera: &Camera) -> error::Result<()> {
        self.renderer.borrow_mut().render(driver, self, camera)
    }

    pub fn release_resources(&mut self, driver: &mut dyn RenderDriver) {
        self.renderer.borrow_mut().release_resources(driver);
    }
}

pub struct RSUpdater<'a> {
    scene: &'a mut RenderScene
}

impl<'a> RSUpdater<'a> {
    pub fn set_cell(&mut self, x: u32, y: u32, cell: CellFill) -> error::Result<()> {
        self.scene.set_cell(x, y, cell)
    }
}

impl<'a> Drop for RSUpdater<'a> {
    fn drop(&mut self) {
        self.scene.end_update();
    }
}
