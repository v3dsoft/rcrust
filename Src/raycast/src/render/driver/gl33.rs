use std::rc::Rc;
use std::cell::RefCell;

mod camera;
use self::camera::CameraData;

mod material;
use self::material::StaticMaterial;
use self::material::static_mat::{ StaticColorMatGL33, StaticColorTexMatGL33 };

mod qstream;
use self::qstream::QuadStreamGL33;

mod shader;
use self::shader::ShaderRegistry;

mod texture;
use self::texture::TextureRegistry;

use crate::error;
use crate::math::{Color, Point3D, Vector3D};
use crate::engine::{
    RenderParams,
    Resolution,
    ResolutionList
};

use crate::render::driver::{
    RenderDriver,
    StaticMatId,
    QuadStreamRender
};

use crate::utils::resources::ResRegistry;

use super::image::reader::{ ImageReader, ImageFormat };

use crate::utils::sdl::SdlInstance;
use sdl2;

use gl;
use gl::types::*;

const MIN_RESOLUTION: Resolution = Resolution {
    width: 640,
    height: 480
};

#[derive(Copy, Clone)]
pub enum VtxAttribute {
    Position = 0,
    TexCoord = 1
}

type StaticMatRegistry = ResRegistry<StaticMatId, Box<dyn StaticMaterial>>;

pub struct RenderDriverGL33 {
    sdl: SdlInstance,
    video: sdl2::VideoSubsystem,

    res_list: ResolutionList,

    window: sdl2::video::Window,
    context: sdl2::video::GLContext,

    params: RenderParams,

    cam_data: Option<CameraData>,

    static_mat_reg: StaticMatRegistry,
    shader_reg: Rc<RefCell<ShaderRegistry>>,
    texture_reg: Rc<RefCell<TextureRegistry>>,

    qstream: QuadStreamGL33
}

fn resolution_exists(res_list: &ResolutionList, width: u32, height: u32) -> bool {
    res_list.iter().any(|res| (res.width == width) && (res.height == height))
}

fn get_supported_resolutions(video: &sdl2::VideoSubsystem) -> error::Result<ResolutionList> {
    let res_count = match video.num_display_modes(0) {
        Ok(count) => count,
        Err(sdl_err) => return Err(error::Error::new(
            error::Cause::Internal,
            &format!("Cannot retrieve video modes count.\nError: {}", sdl_err)
        ))
    };

    let mut res_list = Vec::with_capacity(res_count as usize);

    for mode_id in 0..res_count {
        let mode = match video.display_mode(0, mode_id) {
            Ok(mode) => mode,
            Err(sdl_err) => return Err(error::Error::new(
                error::Cause::Internal,
                &format!("Cannot retrieve video mode {}.\nError: {}", mode_id, sdl_err)
            ))
        };

        if mode.format.byte_size_per_pixel() == 4 &&
            mode.w as u32 >= MIN_RESOLUTION.width &&
            mode.h as u32 >= MIN_RESOLUTION.height
        {
            if !resolution_exists(&res_list, mode.w as u32, mode.h as u32) {
                let res = Resolution {
                    width: mode.w as u32,
                    height: mode.h as u32
                };

                res_list.push(res);
            }
        }
    }

    Ok(res_list)
}

fn init_gl_options(video: &sdl2::VideoSubsystem) {
    let gl_attr = video.gl_attr();

    gl_attr.set_red_size(8);
    gl_attr.set_green_size(8);
    gl_attr.set_blue_size(8);

    gl_attr.set_depth_size(24);
    gl_attr.set_stencil_size(8);

    gl_attr.set_double_buffer(true);
    gl_attr.set_multisample_buffers(0); //TODO: Add multisampling support

    gl_attr.set_context_version(3, 3);
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);

    gl_attr.set_share_with_current_context(true);
}

fn init_window(video: &sdl2::VideoSubsystem, params: &RenderParams) -> error::Result<sdl2::video::Window> {
    let mut window = video.window(&params.title, params.resolution.width, params.resolution.height);
    window.opengl();
    window.position_centered();

    if params.fullscreen {
        window.fullscreen();
    }

    match window.build() {
        Ok(wnd) => Ok(wnd),
        Err(sdl_err) => Err(error::Error::new(
            error::Cause::Internal,
            &format!("Cannot create SDL window.\nError: {}", sdl_err)
        ))
    }
}

fn init_display_mode(video: &sdl2::VideoSubsystem, window: &mut sdl2::video::Window, params: &RenderParams) -> error::Result<()> {
    let sdl_mode = sdl2::video::DisplayMode {
        w: params.resolution.width as i32,
        h: params.resolution.height as i32,
        format: sdl2::pixels::PixelFormatEnum::RGB888,
        refresh_rate: 0
    };
    let sdl_mode = match video.closest_display_mode(0, &sdl_mode) {
        Ok(sdl_mode) => sdl_mode,
        Err(sdl_err) => return Err(error::Error::new(
            error::Cause::Internal,
            &format!("Cannot find closest SDL display mode.\nError: {}", sdl_err)
        ))
    };

    match window.set_display_mode(sdl_mode) {
        Ok(_) => Ok(()),
        Err(sdl_err) => Err(error::Error::new(
            error::Cause::Internal,
            &format!("Cannot set window display mode.\nError: {}", sdl_err)
        ))
    }
}

#[allow(unused_must_use)]
fn init_gl_context(video: &sdl2::VideoSubsystem, window: &sdl2::video::Window, params: &RenderParams) -> error::Result<sdl2::video::GLContext> {
    let context = match window.gl_create_context() {
        Ok(context) => context,
        Err(sdl_err) => return Err(error::Error::new(
            error::Cause::Internal,
            &format!("Cannot create OpenGL context.\nError: {}", sdl_err)
        ))
    };

    if params.vsync {
        video.gl_set_swap_interval(1);
    }

    Ok(context)
}

fn load_gl_funcs(video: &sdl2::VideoSubsystem) {
    gl::load_with(|name| video.gl_get_proc_address(name) as *const std::ffi::c_void);
}

fn set_initial_gl_params() {
    unsafe {
        gl::ClearColor(0.078, 0.067, 0.102, 1.0);

        gl::Disable(gl::CULL_FACE);

        gl::DepthFunc(gl::LEQUAL);
    }
}

impl RenderDriverGL33 {
    pub fn new(params: &RenderParams) -> error::Result<Self> {
        if (params.resolution.width < MIN_RESOLUTION.width) || (params.resolution.height < MIN_RESOLUTION.height) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("params.resolution")),
                &format!("Resolution can not be smaller than 640x480.\nResolution: {}x{}", params.resolution.width, params.resolution.height)
            ));
        }

        let sdl = SdlInstance::new()?;
        let video = sdl.init_video()?;

        let mut params = (*params).clone();

        let res_list = get_supported_resolutions(&video)?;
        if !resolution_exists(&res_list, params.resolution.width, params.resolution.height) {
            //Fall back to the safe resolution
            params.resolution= MIN_RESOLUTION.clone();
        }

        init_gl_options(&video);
        let mut window = init_window(&video, &params)?;
        if params.fullscreen {
            init_display_mode(&video, &mut window, &params)?;
        }

        let context = init_gl_context(&video, &window, &params)?;
        load_gl_funcs(&video);
        set_initial_gl_params();

        let static_mat_reg = StaticMatRegistry::new();
        let shader_reg = Rc::new(RefCell::new(ShaderRegistry::new()));
        let texture_reg = Rc::new(RefCell::new(TextureRegistry::new()));

        let qstream = QuadStreamGL33::new()?;

        Ok(RenderDriverGL33 {
            sdl,
            video,

            res_list,

            window,
            context,

            params,

            cam_data: None,

            static_mat_reg,
            shader_reg,
            texture_reg,

            qstream
        })
    }
}

impl RenderDriver for RenderDriverGL33 {
    fn register_image_format(&mut self, fmt: Rc<dyn ImageFormat>) {
        self.texture_reg.borrow_mut().register_image_format(fmt);
    }

    fn clear(&mut self, color: bool, depth: bool) {
        let mut flags: GLbitfield = 0;
        if color {
            flags = gl::COLOR_BUFFER_BIT;
        }
        if depth {
            flags |= gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT;
        }

        if flags != 0 {
            unsafe {
                gl::Clear(flags);
            }
        }
    }

    fn present(&mut self) {
        unsafe { gl::Finish(); }
        self.window.gl_swap_window();
    }

    fn enable_depth_test(&mut self, enable: bool) {
        unsafe {
            gl::Enable(gl::DEPTH_TEST);
        }
    }

    fn params(&self) -> &RenderParams { &self.params }

    fn create_static_color_material(&mut self, color: &Color) -> error::Result<StaticMatId> {
        let new_mat = Box::new(StaticColorMatGL33::new(self.shader_reg.clone(), color)?);

        Ok(self.static_mat_reg.insert_item(new_mat))
    }

    fn create_static_color_tex_material(&mut self, color: &Color, texture: &str) -> error::Result<StaticMatId> {
        let new_mat = Box::new(StaticColorTexMatGL33::new(self.shader_reg.clone(), self.texture_reg.clone(), color, texture)?);

        Ok(self.static_mat_reg.insert_item(new_mat))
    }

    fn acquire_static_material(&mut self, id: StaticMatId) -> error::Result<u32> {
        self.static_mat_reg.acquire_item(id)
    }

    fn release_static_material(&mut self, id: StaticMatId) -> error::Result<u32> {
        self.static_mat_reg.release_item(id)
    }

    fn set_camera_persp(&mut self, eye: &Point3D, dir: &Vector3D, up: &Vector3D, fov_v: f32, aspect: f32, z_near: f32, z_far: f32) -> error::Result<()> {
        self.cam_data = Some(CameraData::new_perspective(eye, dir, up, fov_v, aspect, z_near, z_far)?);

        Ok(())
    }

    fn begin_quad_render(&mut self, material: StaticMatId) -> error::Result<QuadStreamRender>
    {
        let cam_data = match &self.cam_data {
            Some(cam_data) => cam_data,
            None => return Err(error::Error::new(
                error::Cause::WrongSetup,
                "Camera must be set before trying to draw anything."
            ))
        };

        let mat = self.static_mat_reg.borrow_item_mut(material)?;
        mat.set_current(cam_data)?;

        self.qstream.init()?;

        Ok(QuadStreamRender{
            stream: &mut self.qstream
        })
    }
}