pub mod reader;
mod formats;

use std::alloc;
use std::slice;

use crate::error;
use crate::math::{ PixelSize, ARGB};

pub struct Image {
    size: PixelSize,
    data: *mut ARGB
}

pub struct ImageReadLock<'a> {
    image: &'a Image
}

pub struct ImageWriteLock<'a> {
    image: &'a mut Image
}

impl Image {
    pub fn new(size: &PixelSize) -> error::Result<Image> {
        if (size.width == 0) || (size.height == 0) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("size")),
                "Image size must be greater than 0."
            ));
        }

        let data = Self::init_img_data(&size);

        Ok(Image {
            size: size.clone(),
            data
        })
    }

    fn init_img_data_layout(size: &PixelSize) -> alloc::Layout {
        let data_size = (size.width * size.height * 4) as usize;

        alloc::Layout::from_size_align(data_size, 4).unwrap()
    }

    fn init_img_data(size: &PixelSize) -> *mut ARGB {
        let layout = Self::init_img_data_layout(size);

        unsafe {
            alloc::alloc(layout) as *mut ARGB
        }
    }

    pub fn size(&self) -> &PixelSize {
        &self.size
    }

    pub fn begin_read(&self) -> ImageReadLock {
        ImageReadLock {
            image: self
        }
    }

    pub fn begin_write(&mut self) -> ImageWriteLock {
        ImageWriteLock {
            image: self
        }
    }
}

impl Drop for Image {
    fn drop(&mut self) {
        let layout = Self::init_img_data_layout(&self.size);

        unsafe {
            alloc::dealloc(self.data as *mut u8, layout);
        }
    }
}

impl<'a> ImageReadLock<'a> {
    pub fn data(&self) -> &'a [ARGB] {
        let slice_size = (self.image.size.width * self.image.size.height) as usize;

        unsafe {
            slice::from_raw_parts(self.image.data, slice_size)
        }
    }
}

impl<'a> ImageWriteLock<'a> {
    pub fn data(&self) -> &'a mut [ARGB] {
        let slice_size = (self.image.size.width * self.image.size.height) as usize;

        unsafe {
            slice::from_raw_parts_mut(self.image.data, slice_size)
        }
    }
}