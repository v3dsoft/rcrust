pub mod static_mat;

use crate::error;
use crate::render::driver::gl33::camera::CameraData;

pub trait StaticMaterial {
    fn set_current(&mut self, cam_data: &CameraData) -> error::Result<()>;
}
