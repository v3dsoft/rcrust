use std::fs::File;
use std::ffi::c_void;

use crate::error;
use crate::render::driver::image::Image;
use crate::render::driver::image::reader::ImageReader;
use crate::utils::resources::ResGuard;

pub struct Texture {
    texture: u32
}

impl Texture {
    fn read_image(path: &str, reader: &ImageReader) -> error::Result<Image> {
        let mut img_file = match File::open(path) {
            Ok(img_file) => img_file,
            Err(err) => return Err(error::Error::new(
                error::Cause::FileError,
                &format!("Unable to open image file.\nFile path: {}\nError: {}", path, err)
            ))
        };

        reader.read_image(path, &mut img_file)
    }

    fn create_texture() -> u32 {
        let mut texture: u32 = 0;
        unsafe { gl::GenTextures(1, &mut texture); }

        texture
    }

    fn extract_image_data(image: &Image) -> Vec<u32> {
        let img_size = image.size();
        let img_pixel_count = (img_size.width * img_size.height) as usize;

        let mut data = Vec::with_capacity(img_pixel_count);

        let img_data = image.begin_read();
        let img_data = img_data.data();

        let mut row = 0;
        let mut copy_end = img_pixel_count;
        let mut copy_start = copy_end - (img_size.width as usize);
        while row < img_size.height {
            data.extend_from_slice(&img_data[copy_start..copy_end]);

            row += 1;
            copy_start -= img_size.width as usize;
            copy_end -= img_size.width as usize;
        }

        data
    }

    fn fill_texture(texture: u32, image: &Image) -> error::Result<()> {
        unsafe {
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, texture);

            let img_size = image.size();
            let img_data = Self::extract_image_data(image);

            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                gl::RGBA as i32,
                img_size.width as i32,
                img_size.height as i32,
                0,
                gl::BGRA,
                gl::UNSIGNED_BYTE,
                img_data.as_ptr() as *const c_void
            );
            match gl::GetError() {
                0 => {},
                gl_err => return Err(error::Error::new(
                    error::Cause::Internal,
                    &format!("Unable to setup texture image.\nGL Error: {:X}", gl_err)
                ))
            }
        }

        Ok(())
    }

    pub(super) fn new(path: &str, reader: &ImageReader) -> error::Result<Self> {
        let image = Self::read_image(path, reader)?;

        let texture = ResGuard::new(
            Self::create_texture(),
            |texture| {
                unsafe { gl::DeleteTextures(1, &texture); }
            }
        );

        Self::fill_texture(*texture.borrow(), &image)?;

        Ok(Self {
            texture: texture.take()
        })
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe { gl::DeleteTextures(1, &self.texture); }
    }
}