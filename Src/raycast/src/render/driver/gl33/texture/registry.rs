use std::rc::Rc;
use std::collections::HashMap;

use crate::error;
use crate::utils::resources::{ GenerationResRegistry, GenerationId };
use super::super::{ ImageReader, ImageFormat };

use super::texture::Texture;

#[derive(Copy, Clone)]
pub struct TextureId(GenerationId);

struct TextureRec {
    texture: Texture,
    path: String
}

pub struct TextureRegistry {
    img_reader: ImageReader,

    path_map: HashMap<String, TextureId>,
    data: GenerationResRegistry<TextureRec>
}

impl TextureRegistry {
    pub fn new() -> Self {
        let img_reader = ImageReader::new();

        let path_map = HashMap::new();
        let data = GenerationResRegistry::new();

        Self{
            img_reader,

            path_map,
            data
        }
    }

    pub fn register_image_format(&mut self, fmt: Rc<dyn ImageFormat>) {
        self.img_reader.register_format(fmt);
    }

    pub fn acquire_or_add_texture(&mut self, path: &str) -> error::Result<TextureId> {
        if let Some(tex_id) = self.acquire_texture(path) {
            Ok(tex_id)
        } else {
            self.add_texture(path)
        }
    }

    pub fn add_texture(&mut self, path: &str) -> error::Result<TextureId> {
        if !self.path_map.contains_key(path) {
            let tex = Texture::new(path, &self.img_reader)?;
            let tex_rec = TextureRec {
                texture: tex,
                path: String::from(path)
            };

            let tex_id = TextureId(self.data.insert_item(tex_rec));
            self.path_map.insert(String::from(path), tex_id);

            Ok(tex_id)
        } else {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("path")),
                &format!("Texture with this path alredy registered.\nPath: {}", path)
            ))
        }
    }

    pub fn acquire_texture(&mut self, path: &str) -> Option<TextureId> {
        if let Some(tex_id) = self.path_map.get(path) {
            self.data.acquire_item(tex_id.0);

            Some(*tex_id)
        } else {
            None
        }
    }

    pub fn release_texture(&mut self, id: TextureId) {
        if self.data.item_exists(id.0) {
            let tex_rec = self.data.borrow_item(id.0).expect("Texture must exist here");
            let tex_path = tex_rec.path.clone();

            let tex_ref_count = self.data.release_item(id.0).expect("Texture must be released correctly.");
            if tex_ref_count == 0 {
                self.path_map.remove(&tex_path);
            }
        } else {
            panic!("Attempt to release unexistent texture.");
        }
    }

    pub fn borrow_texture(&self, id: TextureId) -> error::Result<&Texture> {
        match self.data.borrow_item(id.0) {
            Ok(item) => Ok(&item.texture),
            Err(err) => Err(error::Error::new(
                err.cause.clone(),
                &format!("Unable to borrow texture.\nError: {}", err)
            ))
        }
    }

    pub fn borrow_texture_mut(&mut self, id: TextureId) -> error::Result<&mut Texture> {
        match self.data.borrow_item_mut(id.0) {
            Ok(item) => Ok(&mut item.texture),
            Err(err) => Err(error::Error::new(
                err.cause.clone(),
                &format!("Unable to borrow mutable texture.\nError: {}", err)
            ))
        }
    }
}