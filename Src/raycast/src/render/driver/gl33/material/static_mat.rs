pub mod color;
pub use color::StaticColorMatGL33;

pub mod color_tex;
pub use color_tex::StaticColorTexMatGL33;