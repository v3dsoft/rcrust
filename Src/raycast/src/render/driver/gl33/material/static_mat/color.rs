use std::rc::Rc;
use std::cell::RefCell;

use uuid::Uuid;

use crate::error;
use crate::math::{Color, Matrix3D};
use super::super::super::VtxAttribute;
use super::super::super::shader::{ ShaderRegistry, ShaderId, ShaderData };
use crate::render::driver::gl33::camera::CameraData;
use super::super::StaticMaterial;

const STATIC_COLOR_SHADER_UUID: &'static str = "f34ced60-3ce0-11e9-b210-d663bd873d93";

const STATIC_COLOR_SHADER_SRC_VERTEX: &'static str = r#"
#version 330

in vec4 vtxPos;

uniform mat4 mvpMtx;

void main(void) {
    gl_Position = mvpMtx * vtxPos;
}
"#;

const STATIC_COLOR_SHADER_SRC_FRAGMENT: &'static str = r#"
#version 330

uniform vec4 color;

out vec4 fragColor;

void main(void) {
    fragColor = color;
}
"#;

const STATIC_COLOR_SHADER_MVP_NAME: &'static str = "mvpMtx";
const STATIC_COLOR_SHADER_COLOR_NAME: &'static str = "color";

const STATIC_COLOR_SHADER_VTX_POS_NAME: &'static str = "vtxPos";

pub struct StaticColorMatGL33 {
    shader_reg: Rc<RefCell<ShaderRegistry>>,
    shader_id: ShaderId,

    color: Color
}

impl StaticColorMatGL33 {
    fn prepare_shader_data() -> error::Result<ShaderData> {
        let mut shader_data = ShaderData::build();
        shader_data.set_vertex_src(STATIC_COLOR_SHADER_SRC_VERTEX);
        shader_data.set_fragment_src(STATIC_COLOR_SHADER_SRC_FRAGMENT);
        shader_data.set_vertex_attr_binding(VtxAttribute::Position, STATIC_COLOR_SHADER_VTX_POS_NAME);

        shader_data.create()
    }

    pub fn new(shader_reg: Rc<RefCell<ShaderRegistry>>, color: &Color) -> error::Result<Self> {
        let type_id = match Uuid::parse_str(STATIC_COLOR_SHADER_UUID) {
            Ok(id) => id,
            Err(err) => {
                return Err(error::Error::new(
                    error::Cause::Internal,
                    &format!("Unable to parse static color shader type ID.\nError: {:?}", err)
                ));
            }
        };

        let shader_id = {
            let mut shader_reg_ref = shader_reg.borrow_mut();

            match shader_reg_ref.asquire_by_type(type_id) {
                Some(id) => id,
                None => {
                    let shader_data = StaticColorMatGL33::prepare_shader_data()?;
                    shader_reg_ref.add(type_id, &shader_data)?
                }
            }
        };

        Ok(Self {
            shader_reg,
            shader_id,

            color: color.clone()
        })
    }
}

impl StaticMaterial for StaticColorMatGL33 {
    fn set_current(&mut self, cam_data: &CameraData) -> error::Result<()> {
        let mut shader_reg = self.shader_reg.borrow_mut();
        let shader = shader_reg.borrow_mut(self.shader_id)?;

        shader.set_current();
        shader.set_uniform_matrix(STATIC_COLOR_SHADER_MVP_NAME, &cam_data.view_proj);
        shader.set_uniform_color(STATIC_COLOR_SHADER_COLOR_NAME, &self.color);

        Ok(())
    }
}

impl Drop for StaticColorMatGL33 {
    fn drop(&mut self) {
        self.shader_reg.borrow_mut().release(self.shader_id);
    }
}