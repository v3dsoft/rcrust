use std::rc::Rc;
use std::cell::RefCell;

use uuid::Uuid;

use crate::error;
use crate::math::{Color, Matrix3D};
use super::super::super::VtxAttribute;
use super::super::super::shader::{ ShaderRegistry, ShaderId, ShaderData };
use super::super::super::texture::TextureRegistry;
use crate::render::driver::gl33::camera::CameraData;
use crate::render::driver::gl33::texture::TextureId;
use super::super::StaticMaterial;
use crate::utils::resources::ResGuard;

const STATIC_COLOR_TEX_SHADER_UUID: &'static str = "c94c47b0-91a4-4e98-a55d-7c289c2a1b98";

const STATIC_COLOR_TEX_SHADER_SRC_VERTEX: &'static str = r#"
#version 330

in vec4 vtxPos;
in vec2 vtxTex;

uniform mat4 mvpMtx;

smooth out vec2 fragTex;

void main(void) {
    gl_Position = mvpMtx * vtxPos;
    fragTex = vtxTex;
}
"#;

const STATIC_COLOR_TEX_SHADER_SRC_FRAGMENT: &'static str = r#"
#version 330

uniform vec4 color;
uniform sampler2D colorMap;

in vec2 fragTex;

out vec4 fragColor;

void main() {
    vec4 texColor = texture(colorMap, fragTex.st);

    fragColor = color * texColor;
}
"#;

const STATIC_COLOR_TEX_SHADER_MVP_NAME: &'static str = "mvpMtx";
const STATIC_COLOR_TEX_SHADER_COLOR_NAME: &'static str = "color";
const STATIC_COLOR_TEX_SHADER_COLOR_MAP_NAME: &'static str = "colorMap";

const STATIC_COLOR_TEX_SHADER_VTX_POS_NAME: &'static str = "vtxPos";
const STATIC_COLOR_TEX_SHADER_VTX_TEX_NAME: &'static str = "vtxTex";

pub struct StaticColorTexMatGL33 {
    shader_reg: Rc<RefCell<ShaderRegistry>>,
    texture_reg: Rc<RefCell<TextureRegistry>>,

    shader_id: ShaderId,

    color: Color,
    texture_id: TextureId
}

impl StaticColorTexMatGL33 {
    fn prepare_shader_data() -> error::Result<ShaderData> {
        let mut shader_data = ShaderData::build();
        shader_data.set_vertex_src(STATIC_COLOR_TEX_SHADER_SRC_VERTEX);
        shader_data.set_fragment_src(STATIC_COLOR_TEX_SHADER_SRC_FRAGMENT);
        shader_data.set_vertex_attr_binding(VtxAttribute::Position, STATIC_COLOR_TEX_SHADER_VTX_POS_NAME);
        shader_data.set_vertex_attr_binding(VtxAttribute::TexCoord, STATIC_COLOR_TEX_SHADER_VTX_TEX_NAME);

        shader_data.create()
    }

    pub fn new(shader_reg: Rc<RefCell<ShaderRegistry>>, texture_reg: Rc<RefCell<TextureRegistry>>, color: &Color, texture: &str) -> error::Result<Self> {
        let type_id = match Uuid::parse_str(STATIC_COLOR_TEX_SHADER_UUID) {
            Ok(id) => id,
            Err(err) => {
                return Err(error::Error::new(
                    error::Cause::Internal,
                    &format!("Unable to parse colored texture shader type ID.\nError: {:?}", err)
                ));
            }
        };

        let texture_id = ResGuard::new(
            texture_reg.borrow_mut().acquire_or_add_texture(texture)?,
            |texture_id| {
                texture_reg.borrow_mut().release_texture(texture_id);
            }
        );

        let shader_id = {
            let mut shader_reg_ref = shader_reg.borrow_mut();

            match shader_reg_ref.asquire_by_type(type_id) {
                Some(id) => id,
                None => {
                    let shader_data = Self::prepare_shader_data()?;
                    shader_reg_ref.add(type_id, &shader_data)?
                }
            }
        };

        Ok(Self {
            shader_reg,
            texture_reg: texture_reg.clone(),

            shader_id,

            color: color.clone(),
            texture_id: texture_id.take()
        })
    }
}

impl StaticMaterial for StaticColorTexMatGL33 {
    fn set_current(&mut self, cam_data: &CameraData) -> error::Result<()> {
        Err(error::Error::new(
            error::Cause::NotImplemented,
            "Not implemented yet"
        ))
    }
}

impl Drop for StaticColorTexMatGL33 {
    fn drop(&mut self) {
        self.texture_reg.borrow_mut().release_texture(self.texture_id);
        self.shader_reg.borrow_mut().release(self.shader_id);
    }
}
