use crate::error;
use crate::math::{ Matrix3D, Point3D, Vector3D };

pub struct CameraData {
    pub view: Matrix3D,
    pub projection: Matrix3D,
    pub view_proj: Matrix3D
}

impl CameraData {
    pub fn new_perspective(eye: &Point3D, dir: &Vector3D, up: &Vector3D, fov_v: f32, aspect: f32, z_near: f32, z_far: f32) -> error::Result<CameraData> {
        let view = Matrix3D::look_at_dir(eye, dir, up)?;
        let projection = Matrix3D::perspective_gl(fov_v, aspect, z_near, z_far)?;
        let view_proj = &projection * &view;

        Ok(CameraData {
            view,
            projection,
            view_proj
        })
    }
}