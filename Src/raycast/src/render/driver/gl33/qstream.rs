use std::{ ptr, mem };
use std::ffi::c_void;

use crate::error;
use crate::math::{
    Point3D
};
use crate::render::driver::QuadStream;
use super::VtxAttribute;

use crate::utils::resources::ResGuard;

const QS_BUF_VTX_COUNT: usize = 1024;

pub struct QuadStreamGL33 {
    vao: u32,
    vbo: u32,

    data_ptr: *mut u8,
    data_verts: usize
}

impl QuadStreamGL33 {
    unsafe fn create_array_obj() -> u32 {
        let mut vao = 0u32;
        gl::GenVertexArrays(1, &mut vao);

        vao
    }

    unsafe fn create_vertex_obj() -> u32 {
        let mut vbo = 0u32;
        gl::GenBuffers(1, &mut vbo);

        vbo
    }

    unsafe fn setup_buffers(vao: u32, vbo: u32) -> error::Result<()> {
        gl::BindVertexArray(vao);
        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);

        let item_size = mem::size_of::<f32>() * 4;
        let vbo_size = item_size * QS_BUF_VTX_COUNT;

        gl::BufferData(gl::ARRAY_BUFFER, vbo_size as isize, ptr::null(), gl::STREAM_DRAW);
        match gl::GetError() {
            0 => {},
            gl_err => return Err(error::Error::new(
                error::Cause::Internal,
                &format!(
                    "Unable to allocate vertex buffer.\nSize: {}\nGL Error: {}",
                    vbo_size,
                    gl_err
                )
            ))
        }

        gl::EnableVertexAttribArray(VtxAttribute::Position as u32);
        gl::VertexAttribPointer(
            VtxAttribute::Position as u32,
            4,
            gl::FLOAT,
            gl::FALSE,
            item_size as i32,
            0 as *const c_void
        );
        match gl::GetError() {
            0 => {},
            gl_err => return Err(error::Error::new(
                error::Cause::Internal,
                &format!(
                    "Unable to setup position attribute pointer.\nGL Error: {}",
                    gl_err
                )
            ))
        }

        Ok(())
    }

    pub fn new() -> error::Result<QuadStreamGL33> {
        unsafe {
            let vao = ResGuard::new(Self::create_array_obj(), |vao| {
                unsafe { gl::DeleteBuffers(1, &vao); }
            });
            let vbo = ResGuard::new(Self::create_vertex_obj(), |vbo| {
                unsafe { gl::DeleteBuffers(1, &vbo); }
            });

            Self::setup_buffers(*vao.borrow(), *vao.borrow())?;

            Ok(QuadStreamGL33 {
                vao: vao.take(),
                vbo: vbo.take(),

                data_ptr: ptr::null_mut(),
                data_verts: 0
            })
        }
    }

    pub fn init(&mut self) -> error::Result<()> {
        if !self.data_ptr.is_null() {
            return Err(error::Error::new(
                error::Cause::Internal,
                "Draw process already started."
            ));
        }

        unsafe {
            gl::BindVertexArray(self.vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);

            self.data_ptr = gl::MapBuffer(gl::ARRAY_BUFFER, gl::WRITE_ONLY) as *mut u8;
            if self.data_ptr.is_null() {
                return Err(match gl::GetError() {
                    0 => error::Error::new(
                        error::Cause::Internal,
                        "Unable to map vertex buffer.\nGL Error: UNKNOWN"
                    ),
                    gl_err => error::Error::new(
                        error::Cause::Internal,
                        &format!(
                            "Unable to map vertex buffer.\nGL Error: {}",
                            gl_err
                        )
                    )
                })
            }

            self.data_verts = 0;
        }

        Ok(())
    }

    fn flush(&mut self, reinit: bool) -> error::Result<()> {
        unsafe {
            gl::UnmapBuffer(gl::ARRAY_BUFFER);
            if self.data_verts > 0 {
                gl::DrawArrays(gl::TRIANGLES, 0, self.data_verts as i32);
            }

            if reinit {
                self.data_ptr = gl::MapBuffer(gl::ARRAY_BUFFER, gl::WRITE_ONLY) as *mut u8;
                if self.data_ptr.is_null() {
                    return Err(match gl::GetError() {
                        0 => error::Error::new(
                            error::Cause::Internal,
                            "Unable to map vertex buffer.\nGL Error: UNKNOWN"
                        ),
                        gl_err => error::Error::new(
                            error::Cause::Internal,
                            &format!(
                                "Unable to map vertex buffer.\nGL Error: {}",
                                gl_err
                            )
                        )
                    })
                }
            } else {
                self.data_ptr = ptr::null_mut();
            }

            self.data_verts = 0;
        }

        Ok(())
    }
}

unsafe fn write_point(dst: *mut f32, pt: &Point3D) -> *mut f32 {
    let mut write_buf = dst;

    *write_buf = pt.x;
    write_buf = write_buf.offset(1);

    *write_buf = pt.y;
    write_buf = write_buf.offset(1);

    *write_buf = pt.z;
    write_buf = write_buf.offset(1);

    *write_buf = 1.0;
    write_buf = write_buf.offset(1);

    write_buf
}

impl QuadStream for QuadStreamGL33 {
    fn push_simple_quad(&mut self, data: &[Point3D; 4]) -> error::Result<()> {
        const QUAD_VTX_COUNT: usize = 6;  //Two triangles
        const QUAD_FLOAT_COUNT: usize = QUAD_VTX_COUNT * 4;   //4 components per vertex

        if self.data_verts + QUAD_VTX_COUNT > QS_BUF_VTX_COUNT {
            self.flush(true)?;
        }

        unsafe {
            let mut write_buf = self.data_ptr as *mut f32;

            write_buf = write_point(write_buf, &data[0]);
            write_buf = write_point(write_buf, &data[1]);
            write_buf = write_point(write_buf, &data[2]);

            write_buf = write_point(write_buf, &data[0]);
            write_buf = write_point(write_buf, &data[2]);
            write_buf = write_point(write_buf, &data[3]);

            self.data_ptr = write_buf as *mut u8;
        }

        self.data_verts += QUAD_VTX_COUNT;

        Ok(())
    }

    fn finish(&mut self) -> error::Result<()> {
        self.flush(false)
    }
}

impl Drop for QuadStreamGL33 {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
        }
    }
}