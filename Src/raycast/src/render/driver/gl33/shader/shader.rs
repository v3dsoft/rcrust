use std::ffi::CString;
use std::collections::hash_map::HashMap;

use uuid::Uuid;
use gl;
use std::ptr;

use crate::error;
use crate::utils::resources::ResGuard;
use crate::math::{Matrix3D, Color};

use super::super::VtxAttribute;

pub type AttrList = HashMap<u32, String>;

pub struct ShaderData {
    vertex_src: String,
    fragment_src: String,
    attr_list: AttrList
}

impl ShaderData {
    pub fn build() -> ShaderDataBuilder {
        ShaderDataBuilder::new()
    }
}

pub struct ShaderDataBuilder {
    vertex_src: Option<String>,
    fragment_src: Option<String>,
    attr_list: AttrList
}

impl ShaderDataBuilder {
    fn new() -> ShaderDataBuilder {
        let attr_list = HashMap::new();

        ShaderDataBuilder {
            vertex_src: None,
            fragment_src: None,
            attr_list
        }
    }

    pub fn set_vertex_src(&mut self, src: &str) {
        self.vertex_src = Some(String::from(src));
    }

    pub fn set_fragment_src(&mut self, src: &str) {
        self.fragment_src = Some(String::from(src));
    }

    pub fn set_vertex_attr_binding(&mut self, attr: VtxAttribute, var_name: &str) {
        self.attr_list.insert(attr as u32, String::from(var_name));
    }

    pub fn create(self) -> error::Result<ShaderData> {
        if self.vertex_src.is_none() {
            return Err(error::Error::new(
                error::Cause::WrongSetup,
                "Vertex shader source is not set."
            ));
        }

        if self.fragment_src.is_none() {
            return Err(error::Error::new(
                error::Cause::WrongSetup,
                "Fragment shader source is not set."
            ));
        }

        if !self.attr_list.contains_key(&(VtxAttribute::Position as u32)) {
            return Err(error::Error::new(
                error::Cause::WrongSetup,
                "Position attribute binding is not set."
            ));
        }

        Ok(ShaderData {
            vertex_src: self.vertex_src.unwrap(),
            fragment_src: self.fragment_src.unwrap(),
            attr_list: self.attr_list
        })
    }
}

type UniformMap = HashMap<String, i32>;

pub struct Shader {
    type_id: Uuid,
    program: u32,
    uniform_map: UniformMap
}

impl Shader {
    unsafe fn get_shader_info_log(shader: u32) -> String {
        const BUF_SIZE: usize = 1024;

        let mut log_size = 0i32;
        let mut log_buf = [0u8; BUF_SIZE];
        gl::GetShaderInfoLog(shader, BUF_SIZE as i32, &mut log_size, log_buf.as_mut_ptr() as *mut i8);

        let log_cstr = CString::new(&log_buf[0..log_size as usize]).unwrap();

        String::from(log_cstr.to_string_lossy())
    }

    unsafe fn prepare_shader(shader_type: u32, shader_src: &str) -> error::Result<u32> {
        let shader_guard = ResGuard::new(gl::CreateShader(shader_type), |shader_vtx| {
            unsafe { gl::DeleteShader(shader_vtx); }
        });
        let shader = shader_guard.borrow();

        let vtx_src = CString::new(shader_src).unwrap();
        let vtx_src = vec![vtx_src.as_ptr()];

        gl::ShaderSource(*shader, 1, vtx_src.as_ptr(), ptr::null());
        match gl::GetError() {
            0 => {},
            gl_err => return Err(error::Error::new(
                error::Cause::Internal,
                &format!(
                    "Unable to upload shader source.\nShader type: {:X}\nGL Error: {:X}",
                    shader_type,
                    gl_err
                )
            ))
        }

        let mut comp_status = 0i32;
        gl::CompileShader(*shader);
        gl::GetShaderiv(*shader, gl::COMPILE_STATUS, &mut comp_status);
        if comp_status == gl::FALSE as i32 {
            let log = Self::get_shader_info_log(*shader);

            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("shader_src")),
                &format!(
                    "Shader compilation failed.\nShader type: {:X}\nLog:\n{}",
                    shader_type,
                    log
                )
            ));
        }

        Ok(shader_guard.take())
    }

    unsafe fn get_program_info_log(program: u32) -> String {
        const BUF_SIZE: usize = 1024;

        let mut log_size = 0i32;
        let mut log_buf = [0u8; BUF_SIZE];
        gl::GetProgramInfoLog(program, BUF_SIZE as i32, &mut log_size, log_buf.as_mut_ptr() as *mut i8);

        let log_cstr = CString::new(&log_buf[0..log_size as usize]).unwrap();

        String::from(log_cstr.to_string_lossy())
    }

    unsafe fn bind_attributes(program: u32, attr_list: &HashMap<u32, String>) -> error::Result<()> {
        for (attr, var_name) in attr_list.iter() {
            let var_name_c = CString::new(var_name.as_str()).unwrap();
            gl::BindAttribLocation(program, *attr, var_name_c.as_ptr());

            match gl::GetError() {
                0 => {},
                gl_err => return Err(error::Error::new(
                    error::Cause::Internal,
                    &format!(
                        "Unable to bind vertex attribute.\nAttribute: {}\nVariable name: {}\nGL Error: {:X}",
                        attr,
                        var_name,
                        gl_err
                    )
                ))
            }
        }

        Ok(())
    }

    unsafe fn prepare_program(shader_vtx: u32, shader_frag: u32, attr_list: &HashMap<u32, String>) -> error::Result<u32> {
        let program = ResGuard::new(gl::CreateProgram(), |program| {
            gl::DeleteProgram(program);
        });
        let program_id = program.borrow();

        gl::AttachShader(*program_id, shader_vtx);
        gl::AttachShader(*program_id, shader_frag);

        Self::bind_attributes(*program_id, attr_list)?;

        let mut link_status = 0i32;
        gl::LinkProgram(*program_id);
        gl::GetProgramiv(*program_id, gl::LINK_STATUS, &mut link_status);
        if link_status == gl::FALSE as i32 {
            let log = Self::get_program_info_log(*program_id);

            return Err(error::Error::new(
                error::Cause::WrongSetup,
                &format!("Program link failed.\nLog:\n{}", log)
            ));
        }

        Ok(program.take())
    }

    pub(super) fn new(type_id: Uuid, data: &ShaderData) -> error::Result<Shader> {
        unsafe {
            let shader_vtx = ResGuard::new(Self::prepare_shader(gl::VERTEX_SHADER, &data.vertex_src)?, |shader| {
                unsafe { gl::DeleteShader(shader); }
            });
            let shader_frag = ResGuard::new(Self::prepare_shader(gl::FRAGMENT_SHADER, &data.fragment_src)?, |shader| {
                unsafe { gl::DeleteShader(shader); }
            });

            let program = ResGuard::new(
                Self::prepare_program(*shader_vtx.borrow(), *shader_frag.borrow(), &data.attr_list)?,
                |program| {
                    unsafe { gl::DeleteProgram(program); }
            });

            let uniform_map = UniformMap::new();

            Ok(Shader {
                type_id,
                program: program.take(),
                uniform_map
            })
        }
    }

    pub fn type_id(&self) -> Uuid {
        self.type_id
    }

    pub fn set_current(&self) {
        unsafe { gl::UseProgram(self.program); }
    }

    fn find_uniform(&mut self, name: &str) -> i32 {
        //Try to find cached position
        if let Some(pos) = self.uniform_map.get(name) {
            return *pos;
        }

        //Nothing in cache, find in shader
        unsafe {
            let uniform_name = CString::new(name).unwrap();
            let pos = gl::GetUniformLocation(self.program, uniform_name.as_ptr());

            self.uniform_map.insert(String::from(name), pos);

            pos
        }
    }

    pub fn set_uniform_matrix(&mut self, name: &str, mtx: &Matrix3D) {
        let pos = self.find_uniform(name);
        if pos < 0 {
            return;
        }

        unsafe { gl::UniformMatrix4fv(pos, 1, gl::TRUE, &mtx.m11); }
    }

    pub fn set_uniform_color(&mut self, name: &str, color: &Color) {
        let pos = self.find_uniform(name);
        if pos < 0 {
            return;
        }

        unsafe {
            let buf = [
                color.r,
                color.g,
                color.b,
                color.a
            ];

            gl::Uniform4fv(pos, 1, buf.as_ptr());
        }
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe { gl::DeleteProgram(self.program); }
    }
}
