use std::collections::hash_map::HashMap;

use uuid::Uuid;

use crate::error;
use crate::utils::resources::ResRegistry;

use super::{ Shader, ShaderData };

#[derive(Copy, Clone)]
pub struct ShaderId(u32);

impl From<usize> for ShaderId {
    fn from(src: usize) -> Self {
        ShaderId(src as u32)
    }
}

impl Into<usize> for ShaderId {
    fn into(self) -> usize {
        self.0 as usize
    }
}

pub struct ShaderRegistry {
    data: ResRegistry<ShaderId, Shader>,
    type_map: HashMap<Uuid, ShaderId>
}

impl ShaderRegistry {
    pub fn new() -> ShaderRegistry {
        let data = ResRegistry::new();
        let type_map = HashMap::new();

        ShaderRegistry {
            data,
            type_map
        }
    }

    pub fn add(&mut self, type_id: Uuid, data: &ShaderData) -> error::Result<ShaderId> {
        if !self.type_map.contains_key(&type_id) {
            let shader = Shader::new(type_id, data)?;

            let id = self.data.insert_item(shader);
            self.type_map.insert(type_id, id);

            Ok(id)
        } else {
            Err(error::Error::new(
                error::Cause::WrongArgument(String::from("type_id")),
                &format!("Shader with this type ID already registered.\nType ID: {}", type_id)
            ))
        }
    }

    pub fn asquire_by_type(&mut self, type_id: Uuid) -> Option<ShaderId> {
        match self.type_map.get(&type_id) {
            Some(id) => {
                self.data.acquire_item(*id);

                Some(*id)
            },

            None => None
        }
    }

    pub fn borrow_mut(&mut self, id: ShaderId) -> error::Result<&mut Shader> {
        self.data.borrow_item_mut(id)
    }

    pub fn release(&mut self, id: ShaderId) {
        let type_id = match self.data.try_borrow_item(id) {
            Some(shader) => {
                shader.type_id()
            },

            None => panic!("Attempt to release unexistent shader.")
        };

        let ref_count = self.data.release_item(id).expect("Attempt to release unexistent shader.");
        if ref_count == 0 {
            self.type_map.remove_entry(&type_id);
        }
    }
}