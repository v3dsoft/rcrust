use std::rc::Rc;
use std::io::Read;
use std::collections::VecDeque;

use crate::error;
use super::Image;

use super::formats::png::ImageFormatPng;

pub trait ImageFormat {
    fn supported_image(&self, path: &str) -> bool;
    fn read_image(&self, data: &mut dyn Read) -> error::Result<Image>;
}

pub(crate) struct ImageReader {
    format_stack: VecDeque<Rc<dyn ImageFormat>>
}

impl ImageReader {
    pub fn new() -> ImageReader {
        let mut reader = ImageReader {
            format_stack: VecDeque::new()
        };

        reader.register_format(Rc::new(ImageFormatPng::new()));

        reader
    }

    pub fn register_format(&mut self, fmt: Rc<dyn ImageFormat>) {
        self.format_stack.push_front(fmt);
    }

    pub fn read_image(&self, path: &str, data: &mut dyn Read) -> error::Result<Image> {
        for fmt in &self.format_stack {
            if fmt.supported_image(path) {
                return match fmt.read_image(data) {
                    Ok(image) => Ok(image),
                    Err(err) => Err(error::Error::new(
                        error::Cause::WrongFormat,
                        &format!("Unable to read image.\nPath: {}\nError: {}", path, err)
                    ))
                }
            }
        }

        Err(error::Error::new(
            error::Cause::WrongSetup,
            &format!("There is no compliant image format registered.\nPath: {}", path)
        ))
    }
}
