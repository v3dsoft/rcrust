use std::io::Read;

use crate::error;
use crate::math::{PixelSize, ARGB, argb_from_components};
use crate::utils::path::extract_ext;

use super::super::Image;
use super::super::reader::ImageFormat;

use png::{Decoder, OutputInfo, ColorType, BitDepth};

const PNG_EXT: &'static str = ".png";

pub struct ImageFormatPng {}

impl ImageFormatPng {
    pub fn new() -> Self {
        Self {}
    }

    fn supported_color_type(info: &OutputInfo) -> bool {
        ((info.color_type == ColorType::RGB) || (info.color_type == ColorType::RGBA)) && (info.bit_depth == BitDepth::Eight)
    }

    fn create_image(info: &OutputInfo, buf: &Vec<u8>) -> error::Result<Image> {
        let mut image = Image::new(&PixelSize {
            width: info.width,
            height: info.height
        })?;

        let image_data = image.begin_write();
        let mut image_data = image_data.data();

        match info.color_type {
            ColorType::RGB => unsafe {
                let src_row_size = (info.width * 3) as usize;
                if info.line_size < src_row_size {
                    return Err(error::Error::new(
                        error::Cause::Internal,
                        &format!("PNG data line is too small.\nMin line size: {}\nActual line size: {}", src_row_size, info.line_size)
                    ));
                }

                let src_row_stride = info.line_size - src_row_size;

                let mut buf_item = 0usize;
                let mut x = 0u32;
                for pixel in image_data {
                    let r = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    let g = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    let b = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    *pixel = argb_from_components(255, r, g, b);

                    x += 1;
                    if x == info.width {
                        buf_item += src_row_stride;
                        x = 0;
                    }
                }
            },

            ColorType::RGBA => unsafe {
                let src_row_size = (info.width * 4) as usize;
                if info.line_size < src_row_size {
                    return Err(error::Error::new(
                        error::Cause::Internal,
                        &format!("PNG data line is too small.\nMin line size: {}\nActual line size: {}", src_row_size, info.line_size)
                    ));
                }

                let src_row_stride = info.line_size - src_row_size;

                let mut buf_item = 0usize;
                let mut x = 0u32;
                for pixel in image_data {
                    let r = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    let g = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    let b = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    let a = *buf.get_unchecked(buf_item);
                    buf_item += 1;

                    *pixel = argb_from_components(a, r, g, b);

                    x += 1;
                    if x == info.width {
                        buf_item += src_row_stride;
                        x = 0;
                    }
                }
            },

            _ => return Err(error::Error::new(
                error::Cause::WrongFormat,
                &format!("Only 8-bit RGB/RGBA images are supported.")
            ))
        }

        Ok(image)
    }
}

impl ImageFormat for ImageFormatPng {
    fn supported_image(&self, path: &str) -> bool {
        let ext = extract_ext(path);

        ext == PNG_EXT
    }

    fn read_image(&self, data: &mut dyn Read) -> error::Result<Image> {
        let decoder = Decoder::new(data);

        let (info, mut reader) = match decoder.read_info() {
            Ok(info) => info,
            Err(err) => return Err(error::Error::new(
                error::Cause::WrongFormat,
                &format!("Unable to read PNG file info.\nError: {}", err)
            ))
        };

        if !Self::supported_color_type(&info) {
            return Err(error::Error::new(
                error::Cause::WrongFormat,
                &format!("Only 8-bit RGB/RGBA images are supported.")
            ));
        }

        let mut buf = vec![0; info.buffer_size()];
        if let Err(err) = reader.next_frame(&mut buf) {
            return Err(error::Error::new(
                error::Cause::WrongFormat,
                &format!("Unable to read PNG image data.\nError: {}", err)
            ))
        }

        Self::create_image(&info, &buf)
    }
}