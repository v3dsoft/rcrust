pub(crate) mod gl33;

pub mod image;

use std::rc::Rc;

use crate::error;
use crate::engine::RenderParams;

use crate::math::{Point3D, Color, Vector3D};

use self::image::reader::ImageFormat;

#[derive(Copy, Clone)]
pub struct StaticMatId(u32);

impl From<usize> for StaticMatId {
    fn from(src: usize) -> Self {
        StaticMatId(src as u32)
    }
}

impl Into<usize> for StaticMatId {
    fn into(self) -> usize {
        self.0 as usize
    }
}

pub trait RenderDriver {
    fn register_image_format(&mut self, fmt: Rc<dyn ImageFormat>);

    fn clear(&mut self, color: bool, depth: bool);
    fn present(&mut self);

    fn enable_depth_test(&mut self, enable: bool);

    fn params(&self) -> &RenderParams;

    fn create_static_color_material(&mut self, color: &Color) -> error::Result<StaticMatId>;
    fn create_static_color_tex_material(&mut self, color: &Color, texture: &str) -> error::Result<StaticMatId>;
    fn acquire_static_material(&mut self, id: StaticMatId) -> error::Result<u32>;
    fn release_static_material(&mut self, id: StaticMatId) -> error::Result<u32>;

    fn set_camera_persp(&mut self, eye: &Point3D, dir: &Vector3D, up: &Vector3D, fov_v: f32, aspect: f32, z_near: f32, z_far: f32) -> error::Result<()>;

    fn begin_quad_render(&mut self, material: StaticMatId) -> error::Result<QuadStreamRender>;
}

trait QuadStream {
    fn push_simple_quad(&mut self, data: &[Point3D; 4]) -> error::Result<()>;
    fn finish(&mut self) -> error::Result<()>;
}

pub struct QuadStreamRender<'a> {
    stream: &'a mut dyn QuadStream
}

impl<'a> QuadStreamRender<'a> {
    pub fn push_simple_quad(&mut self, data: &[Point3D; 4]) -> error::Result<()> {
        self.stream.push_simple_quad(data)
    }
}

impl<'a> Drop for QuadStreamRender<'a> {
    fn drop(&mut self) {
        self.stream.finish();
    }
}