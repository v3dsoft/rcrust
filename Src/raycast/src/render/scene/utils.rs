use crate::error;

pub struct RenderRegistry {
    data: Vec<u32>,
    frame: u32
}

impl RenderRegistry {
    pub fn new() -> RenderRegistry {
        RenderRegistry {
            data: Vec::new(),
            frame: 1
        }
    }

    pub fn begin_frame(&mut self, size: usize) {
        self.data.resize(size, 0);

        self.frame += 1;
        if self.frame == 0 {
            // In case of wrap around move to 1, because 0 is reserved for new items
            self.frame = 1;
        }
    }

    pub fn item_marked(&self, index: usize) -> bool {
        if index >= self.data.len() {
            // Out-of-bounds items are always marked
            return true;
        }

        self.data[index] == self.frame
    }

    pub fn mark_item(&mut self, index: usize) {
        if index < self.data.len() {
            self.data[index] = self.frame
        }
    }
}

pub struct RenderCatList<Cat: PartialEq + Clone, Item: Clone> {
    data: Vec<Vec<Item>>,
    cat_list: Vec<Cat>
}

impl<Cat: PartialEq + Clone, Item: Clone> RenderCatList<Cat, Item> {
    pub fn new() -> Self {
        let data = Vec::new();
        let cat_list = Vec::new();

        Self {
            data,
            cat_list
        }
    }

    pub fn reset(&mut self) {
        self.cat_list.clear();
    }

    fn prepare_category(&mut self, category: &Cat) -> usize {
        let cat_list_len = self.cat_list.len();

        for cat_id in 0..cat_list_len {
            if self.cat_list[cat_id] == *category {
                return cat_id;
            }
        }

        self.cat_list.push(category.clone());
        if cat_list_len < self.data.len() {
            self.data[cat_list_len].clear();
        } else {
            self.data.push(Vec::new());
        }

        cat_list_len
    }

    pub fn add_item(&mut self, category: &Cat, item: &Item) {
        let cat_id = self.prepare_category(category);
        self.data[cat_id].push(item.clone());
    }

    pub fn iterate_category_err<F>(&self, mut func: F) -> error::Result<()>
        where F: FnMut(&Cat, &Vec<Item>) -> error::Result<()>
    {
        let cat_count = self.cat_list.len();
        for cat_id in 0..cat_count {
            func(&self.cat_list[cat_id], &self.data[cat_id])?;
        }

        Ok(())
    }
}