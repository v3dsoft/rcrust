use std::f32;

use crate::error;
use crate::math::{
    Point3D,
    Vector3D,
    Vector2D,
    Matrix2D,

    EPSILON,
    F32Funcs,
    fov_h_angle_from_v,
    normalize_angle
};

use super::view::View2D;
use crate::render::driver::RenderDriver;

pub struct Camera {
    pos: Point3D,
    h_angle: f32,
    v_angle: f32,

    fov_v_angle: f32,
    aspect: f32
}

const DIR_ZERO: Vector2D = Vector2D {
    x: 1.0,
    y: 0.0
};

impl Camera {
    pub(crate) fn new(pos: &Point3D, h_angle: f32, v_angle: f32, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        if fov_v_angle < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("v_angle")),
                &format!("Vertical FOV angle must be greater than 0.")
            ));
        }

        if aspect < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("aspect")),
                &format!("View aspect must be greater than 0.")
            ));
        }

        Ok(Camera {
            pos: pos.clone(),
            h_angle,
            v_angle,

            fov_v_angle,
            aspect
        })
    }

    pub(crate) fn with_dir(pos: &Point3D, dir: &Vector3D, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        if dir.length_sq() < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("dir")),
                &format!("Length of direction vector should not be 0.\nDirection: {:?}", dir)
            ));
        }

        let dir_2d = dir.discard_z();
        if dir_2d.length_sq() < EPSILON {
            let v_angle = if dir.z > 0.0 { f32::consts::FRAC_PI_2 } else { -f32::consts::FRAC_PI_2 };
            return Self::new(pos, 0.0, v_angle, fov_v_angle, aspect);
        }

        let h_angle = Vector2D::angle_full(&DIR_ZERO, &dir_2d);

        if dir.z.approx_eq(0.0) {
            Self::new(pos, h_angle, 0.0, fov_v_angle, aspect)
        } else {
            let len_2d = dir_2d.length();
            let v_angle = dir.z.atan2(len_2d);

            Self::new(pos, h_angle, v_angle, fov_v_angle, aspect)
        }
    }

    pub(crate) fn with_target(pos: &Point3D, tgt: &Point3D, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        if pos == tgt {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("tgt")),
                &format!(
                    "Eye position and target are the same.\nPosition: {:?}\nTarget: {:?}",
                    pos,
                    tgt
                )
            ));
        }

        let dir = tgt - pos;
        Self::with_dir(pos, &dir, fov_v_angle, aspect)
    }

    pub fn set_position(&mut self, pos: &Point3D) {
        self.pos = pos.clone();
    }

    pub fn position(&self) -> &Point3D {
        &self.pos
    }

    pub fn set_h_angle(&mut self, h_angle: f32) {
        self.h_angle = normalize_angle(h_angle);
    }

    pub fn h_angle(&self) -> f32 { self.h_angle }

    pub fn set_v_angle(&mut self, v_angle: f32) {
        self.v_angle = normalize_angle(v_angle);
    }

    pub fn v_angle(&self) -> f32 { self.v_angle }

    pub fn set_fov_v_angle(&mut self, v_angle: f32) -> error::Result<()> {
        if v_angle < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("v_angle")),
                &format!("Vertical view angle must be greater than 0.")
            ));
        }

        self.fov_v_angle = v_angle;
        Ok(())
    }

    pub fn fov_v_angle(&self) -> f32 { self.fov_v_angle }

    pub fn fov_h_angle(&self) -> f32 {
        fov_h_angle_from_v(self.fov_v_angle, self.aspect)
    }

    pub fn set_aspect(&mut self, aspect: f32) -> error::Result<()> {
        if aspect < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("aspect")),
                &format!("View aspect must be greater than 0.")
            ));
        }

        self.aspect = aspect;
        Ok(())
    }

    pub fn aspect(&self) -> f32 { self.aspect }

    pub fn direction_2d(&self) -> Vector2D {
        let rot_mtx = Matrix2D::rotation(self.h_angle);

        &rot_mtx * &DIR_ZERO
    }

    pub(super) fn view(&self) -> error::Result<View2D> {
        View2D::new(&self.pos.discard_z(), &self.direction_2d(), self.fov_h_angle())
    }

    pub(crate) fn set_current(&self, driver: &mut dyn RenderDriver) -> error::Result<()> {
        let h_cos = self.h_angle.cos();
        let h_sin = self.h_angle.sin();
        let v_cos = self.v_angle.cos();
        let v_sin = self.v_angle.sin();

        let dir = Vector3D {
            x: h_cos * v_cos,
            y: h_sin * v_cos,
            z: v_sin
        };

        let side = Vector3D {
            x: -h_sin,
            y: h_cos,
            z: 0.0
        };

        let up = Vector3D::cross(&dir, &side);

        driver.set_camera_persp(&self.pos, &dir, &up, self.fov_v_angle, self.aspect, 0.5, 100000.0)
    }
}