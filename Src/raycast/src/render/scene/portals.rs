use crate::error;
use crate::math::{
    Point2D,
    AABox2D,

    EPSILON,
    F32Funcs
};
use crate::render::scene::view::View2D;

pub enum PortalFlags {
    N = 0b0001,
    E = 0b0010,
    S = 0b0100,
    W = 0b1000
}

#[derive(Clone)]
pub struct PortalCell {
    bbox: AABox2D,
    portals: u8
}

impl PortalCell {
    pub fn new(origin: &Point2D, cell_size: f32, portals: u8) -> error::Result<PortalCell> {
        if cell_size < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("cell_size")),
                &format!("Cell size must be greater than 0.\nCell size: {}", cell_size)
            ));
        }

        let bbox = AABox2D::new(origin, cell_size);

        Ok(PortalCell {
            bbox,
            portals
        })
    }

    pub fn process_view<F: FnMut(View2D, i32, i32)>(&self, view: &View2D, mut yield_fn: F) {
        let view_origin = view.origin();

        let pt_min = self.bbox.min();
        let pt_max = self.bbox.max();

        // North portal
        if self.portals & (PortalFlags::N as u8) != 0 {
            if view_origin.y.approx_less(pt_max.y) {
                if let Some(new_view) = view.clip_h_portal(pt_max.y, pt_min.x, pt_max.x) {
                    yield_fn(new_view, 0, -1);
                }
            }
        }

        // East portal
        if self.portals & (PortalFlags::E as u8) != 0 {
            if view_origin.x.approx_less(pt_max.x) {
                if let Some(new_view) = view.clip_v_portal(pt_max.x, pt_min.y, pt_max.y) {
                    yield_fn(new_view, 1, 0);
                }
            }
        }

        // South portal
        if self.portals & (PortalFlags::S as u8) != 0 {
            if view_origin.y.approx_greater(pt_min.y) {
                if let Some(new_view) = view.clip_h_portal(pt_min.y, pt_min.x, pt_max.x) {
                    yield_fn(new_view, 0, 1);
                }
            }
        }

        // West portal
        if self.portals & (PortalFlags::W as u8) != 0 {
            if view_origin.x.approx_greater(pt_min.x) {
                if let Some(new_view) = view.clip_v_portal(pt_min.x, pt_min.y, pt_max.y) {
                    yield_fn(new_view, -1, 0);
                }
            }
        }
    }
}