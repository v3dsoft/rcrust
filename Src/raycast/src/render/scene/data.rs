use std::collections::HashMap;
use crate::error;
use crate::math::{
    Point3D,
    Color,
    XRGB
};
use crate::render::driver::{
    RenderDriver,
    StaticMatId
};

use super::{
    RenderScene,
    WallDir,
    CellFill
};
use super::wall::{
    Wall,
    SegmFill
};
use super::utils::{
    RenderRegistry,
    RenderCatList
};
use core::borrow::Borrow;
use crate::render::scene::camera::Camera;

// Temporary colors for planes. This will disappear when proper materials arrive.
const COLOR_FLOOR: Color = Color::new_xrgb(0.514, 0.235, 0.169);
const COLOR_CEILING: Color = Color::new_xrgb(0.071, 0.588, 0.757);
const COLOR_WALL: [Color; 4] = [
    Color::new_xrgb(0.18, 0.212, 0.224),
    Color::new_xrgb(0.357, 0.424, 0.443),
    Color::new_xrgb(0.671, 0.792, 0.831),
    Color::new_xrgb(0.271, 0.318, 0.333)
];

struct MaterialSet {
    floor_mat: StaticMatId,
    ceiling_mat: StaticMatId,
    wall_mat: [StaticMatId; 4],

    mat_map: HashMap<XRGB, StaticMatId>
}

impl MaterialSet {
    fn find_material(&self, color: &Color) -> Option<StaticMatId> {
        let color = color.to_xrgb();

        match self.mat_map.get(&color) {
            Some(id) => Some(*id),
            None => None
        }
    }
}

#[derive(Clone)]
struct HorTile {
    origin: Point3D,
    floor: bool
}

impl HorTile {
    fn verts(&self, cell_size: f32) -> [Point3D; 4] {
        if self.floor {
            [
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y,
                    z: self.origin.z
                },
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y + cell_size,
                    z: self.origin.z
                },
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y + cell_size,
                    z: self.origin.z
                },
                self.origin.clone()
            ]
        } else {
            [
                self.origin.clone(),
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y + cell_size,
                    z: self.origin.z
                },
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y + cell_size,
                    z: self.origin.z
                },
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y,
                    z: self.origin.z
                }
            ]
        }
    }
}

struct HorTileList {
    data: RenderCatList<Color, HorTile>
}

impl HorTileList {
    pub fn new() -> HorTileList {
        let data = RenderCatList::new();

        HorTileList {
            data
        }
    }

    pub fn reset(&mut self) {
        self.data.reset();
    }

    pub fn add_tile(&mut self, origin: &Point3D, floor: bool, color: &Color) {
        self.data.add_item(color, &HorTile {
            origin: origin.clone(),
            floor
        });
    }
}

#[derive(Clone)]
struct VertSegment {
    origin: Point3D,
    height: f32,
    dir: WallDir
}

impl VertSegment {
    fn verts(&self, cell_size: f32) -> [Point3D; 4] {
        match self.dir {
            WallDir::N => [
                self.origin.clone(),
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y,
                    z: self.origin.z + self.height
                },
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y,
                    z: self.origin.z + self.height
                },
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y,
                    z: self.origin.z
                }
            ],
            WallDir::E => [
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y + cell_size,
                    z: self.origin.z
                },
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y + cell_size,
                    z: self.origin.z + self.height
                },
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y,
                    z: self.origin.z + self.height
                },
                self.origin.clone()
            ],
            WallDir::S => [
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y,
                    z: self.origin.z
                },
                Point3D {
                    x: self.origin.x + cell_size,
                    y: self.origin.y,
                    z: self.origin.z + self.height
                },
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y,
                    z: self.origin.z + self.height
                },
                self.origin.clone(),
            ],
            WallDir::W => [
                self.origin.clone(),
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y,
                    z: self.origin.z + self.height
                },
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y + cell_size,
                    z: self.origin.z + self.height
                },
                Point3D {
                    x: self.origin.x,
                    y: self.origin.y + cell_size,
                    z: self.origin.z
                }
            ]
        }
    }
}

struct VertSegmList {
    data: RenderCatList<Color, VertSegment>
}

impl VertSegmList {
    pub fn new() -> VertSegmList {
        let data = RenderCatList::new();

        VertSegmList {
            data
        }
    }

    pub fn reset(&mut self) {
        self.data.reset();
    }

    pub fn add_segment(&mut self, origin: &Point3D, height: f32, dir: WallDir, color: &Color) {
        self.data.add_item(color, &VertSegment {
            origin: origin.clone(),
            height,
            dir
        });
    }
}

pub struct RenderData {
    materials: Option<MaterialSet>,

    htl: HorTileList,
    vsl: VertSegmList,

    wall_rr: RenderRegistry,
}

impl RenderData {
    pub fn new() -> error::Result<RenderData> {
        let htl = HorTileList::new();
        let vsl = VertSegmList::new();

        let wall_rr = RenderRegistry::new();

        Ok(RenderData {
            materials: None,

            htl,
            vsl,

            wall_rr
        })
    }

    pub fn begin_frame(&mut self, scene: &RenderScene) {
        self.htl.reset();
        self.vsl.reset();

        self.wall_rr.begin_frame(scene.wall_data.len());
    }

    fn render_wall(&mut self, wall: &Wall) {
        let mut segm_origin = wall.origin.clone();

        for segm in &wall.segm_list {
            if let SegmFill::Wall { dir } = segm.fill {
                let color = &COLOR_WALL[dir as usize];
                self.vsl.add_segment(&segm_origin, segm.height, dir, color);
            }

            segm_origin.z += segm.height;
        }
    }

    pub fn render_cell(&mut self, scene: &RenderScene, cell_x: u32, cell_y: u32) {
        if let Some(ci) = scene.cell_info(cell_x, cell_y) {
            if let CellFill::Space{floor, ceiling} = ci.fill() {
                let origin = ci.origin();

                // Floor
                let floor_origin = Point3D {
                    x: origin.x,
                    y: origin.y,
                    z: floor.height
                };
                self.htl.add_tile(&floor_origin, true, &COLOR_FLOOR);

                // Ceiling
                let ceil_origin = Point3D {
                    x: origin.x,
                    y: origin.y,
                    z: ceiling.height
                };
                self.htl.add_tile(&ceil_origin, true, &COLOR_CEILING);

                // Walls
                for wall_dir in [WallDir::N, WallDir::E, WallDir::S, WallDir::W].iter() {
                    if let Some((wall_id, wall)) = ci.wall(*wall_dir) {
                        if !self.wall_rr.item_marked(wall_id) {
                            self.render_wall(wall);
                            self.wall_rr.mark_item(wall_id);
                        }
                    }
                }
            }
        }
    }

    fn init_materials(&mut self, driver: &mut dyn RenderDriver) -> error::Result<()> {
        let mut mat_map = HashMap::new();

        let tempMat = driver.create_static_color_tex_material(&Color::new_xrgb(1.0, 1.0, 1.0), "Tex/TestGrid001.png");

        let wall_mat = [
            driver.create_static_color_material(&COLOR_WALL[0])?,
            driver.create_static_color_material(&COLOR_WALL[1])?,
            driver.create_static_color_material(&COLOR_WALL[2])?,
            driver.create_static_color_material(&COLOR_WALL[3])?
        ];
        for i in 0..4 {
            mat_map.insert(COLOR_WALL[i].to_xrgb(), wall_mat[i]);
        }

        let floor_mat = driver.create_static_color_material(&COLOR_FLOOR)?;
        mat_map.insert(COLOR_FLOOR.to_xrgb(), floor_mat);

        let ceiling_mat = driver.create_static_color_material(&COLOR_CEILING)?;
        mat_map.insert(COLOR_CEILING.to_xrgb(), ceiling_mat);

        let _test_mat = driver.create_static_color_tex_material(&Color::new_xrgb(1.0, 1.0, 1.0), "../Tex/TestGrid001.png")?;

        self.materials = Some(MaterialSet {
            floor_mat,
            ceiling_mat,
            wall_mat,

            mat_map
        });

        Ok(())
    }

    fn draw_vert_segments(&self, cell_size: f32, driver: &mut dyn RenderDriver) -> error::Result<()> {
        let materials = match &self.materials {
            Some(materials) => materials,
            None => return Err(error::Error::new(
                error::Cause::Internal,
                "Materials should be initialized at this point."
            ))
        };

        self.vsl.data.iterate_category_err(|color, segm_list| {
            let mat_id = match materials.find_material(color) {
                Some(mat_id) => mat_id,
                None => return Err(error::Error::new(
                    error::Cause::Internal,
                    "Material set should have this material."
                ))
            };

            let mut draw_stream = driver.begin_quad_render(mat_id)?;

            for segm in segm_list {
                let verts = segm.verts(cell_size);
                draw_stream.push_simple_quad(&verts);
            }

            Ok(())
        })?;

        Ok(())
    }

    fn draw_hor_tiles(&self, cell_size: f32, driver: &mut dyn RenderDriver) -> error::Result<()> {
        let materials = match &self.materials {
            Some(materials) => materials,
            None => return Err(error::Error::new(
                error::Cause::Internal,
                "Materials should be initialized at this point."
            ))
        };

        self.htl.data.iterate_category_err(|color, tile_list| {
            let mat_id = match materials.find_material(color) {
                Some(mat_id) => mat_id,
                None => return Err(error::Error::new(
                    error::Cause::Internal,
                    "Material set should have this material."
                ))
            };

            let mut draw_stream = driver.begin_quad_render(mat_id)?;

            for tile in tile_list {
                let verts = tile.verts(cell_size);
                draw_stream.push_simple_quad(&verts);
            }

            Ok(())
        })?;

        Ok(())
    }

    pub fn end_frame(&mut self, cell_size: f32, driver: &mut dyn RenderDriver) -> error::Result<()> {
        if self.materials.is_none() {
            self.init_materials(driver)?;
        }

        self.draw_vert_segments(cell_size, driver)?;
        self.draw_hor_tiles(cell_size, driver)?;

        Ok(())
    }

    pub fn release_resources(&mut self, driver: &mut dyn RenderDriver) {
        if let Some(materials) = &self.materials {
            driver.release_static_material(materials.floor_mat);
            driver.release_static_material(materials.ceiling_mat);
            for wall_mat in materials.wall_mat.iter() {
                driver.release_static_material(*wall_mat);
            }

            self.materials = None;
        }
    }
}