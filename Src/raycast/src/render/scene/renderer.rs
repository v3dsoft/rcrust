use crate::error;
use crate::render::driver::RenderDriver;

use super::{
    RenderScene,

    Camera,
    PointPos2D
};
use super::walker::CellWalker;
use super::data::RenderData;

pub struct SceneRenderer {
    walker: CellWalker,
    data: RenderData
}

impl SceneRenderer {
    pub fn new() -> error::Result<Self> {
        Ok(SceneRenderer {
            walker: CellWalker::new(),
            data: RenderData::new()?
        })
    }

    pub fn render(&mut self, driver: &mut dyn RenderDriver, scene: &RenderScene, camera: &Camera) -> error::Result<()> {
        driver.enable_depth_test(true);

        match scene.classify_point(camera.position()) {
            PointPos2D::Inside(x, y) => self.render_inside(driver, scene, camera, x, y),
            PointPos2D::Outside => self.render_outside(driver, scene, camera)
        }
    }

    fn render_inside(&mut self, driver: &mut dyn RenderDriver, scene: &RenderScene, camera: &Camera, cell_x: u32, cell_y: u32) -> error::Result<()> {
        let walker = &mut self.walker;
        let data = &mut self.data;

        data.begin_frame(scene);

        walker.walk_inside(scene, camera, cell_x, cell_y, |x, y| {
            data.render_cell(scene, x, y);
        });

        camera.set_current(driver)?;
        data.end_frame(scene.cell_size, driver)?;

        Ok(())
    }

    fn render_outside(&mut self, driver: &mut dyn RenderDriver, scene: &RenderScene, camera: &Camera) -> error::Result<()> {
        Ok(())
    }

    pub fn release_resources(&mut self, driver: &mut dyn RenderDriver) {
        self.data.release_resources(driver);
    }
}