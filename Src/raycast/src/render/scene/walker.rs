use std::mem;
use std::cell::RefCell;

use crate::math::linear_coord;

use super::{
    RenderScene,
    Camera
};
use super::view::View2D;
use super::portals::PortalCell;
use super::utils::RenderRegistry;

pub struct CellWalker {
    buf_data: [RefCell<WalkArray>; 2],
    buf_read: usize,
    buf_write: usize,

    cell_rr: RenderRegistry
}

impl CellWalker {
    pub fn new() -> CellWalker {
        let buf_data = [
            RefCell::new(Vec::new()),
            RefCell::new(Vec::new())
        ];

        let cell_rr = RenderRegistry::new();

        CellWalker {
            buf_data,
            buf_read: 0,
            buf_write: 1,

            cell_rr
        }
    }

    pub fn walk_inside<F: FnMut(u32, u32)>(&mut self, scene: &RenderScene, camera: &Camera, start_x: u32, start_y: u32, mut render_cell: F) {
        if (start_x >= scene.width) || (start_y >= scene.height) {
            return;     // This function is for inner cells only
        }

        let cell_count = (scene.width * scene.height) as usize;
        self.cell_rr.begin_frame(cell_count);

        let start_view = camera.view().expect("Camera must have correct parameters");
        let start_pos = WalkPos::new(scene, start_x, start_y);

        self.buf_data[self.buf_read].borrow_mut().push((start_view, start_pos));

        loop {
            loop {
                let (view, pos) = match self.buf_data[self.buf_read].borrow_mut().pop() {
                    Some(data) => data,
                    None => break
                };

                if !self.cell_rr.item_marked(pos.index as usize) {
                    render_cell(pos.x, pos.y);
                    self.cell_rr.mark_item(pos.index as usize);
                }

                pos.pcell.process_view(&view, |new_view, dx, dy| {
                    let cell_x = (pos.x as i32 + dx) as u32;
                    let cell_y = (pos.y as i32 + dy) as u32;

                    let new_pos = WalkPos::new(scene, cell_x, cell_y);
                    self.buf_data[self.buf_write].borrow_mut().push((new_view, new_pos));
                });
            }

            if self.buf_data[self.buf_write].borrow().len() > 0 {
                self.swap_buffers();
            } else {
                break;
            }
        }
    }

    fn swap_buffers(&mut self) {
        self.buf_data[self.buf_read].borrow_mut().clear();
        mem::swap(&mut self.buf_read, &mut self.buf_write);
    }
}

#[derive(Clone)]
pub struct WalkPos {
    pub x: u32,
    pub y: u32,
    pub index: u32,

    pub pcell: PortalCell
}

impl WalkPos {
    pub fn new(scene: &RenderScene, x: u32, y: u32) -> WalkPos {
        let index = linear_coord(x, y, scene.width);
        let pcell = scene.cell_portals(x, y).expect("Cell must be valid");

        WalkPos {
            x,
            y,
            index,

            pcell
        }
    }
}

type WalkArray = Vec<(View2D, WalkPos)>;
