use crate::error;
use crate::math::{
    Point2D,
    Vector2D,
    Ray2D,

    EPSILON,
    F32Funcs
};

const MAX_ANGLE: f32 = 2.79253; //160 degrees

#[derive(Clone)]
pub struct View2D {
    l: Ray2D,
    r: Ray2D
}

impl View2D {
    pub fn new(origin: &Point2D, dir: &Vector2D, fov: f32) -> error::Result<View2D> {
        if (fov < EPSILON) || (fov.approx_greater(MAX_ANGLE)) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("fov")),
                &format!("FOV must be greater than 0 and less or equal than 2.79253 radians (160 degrees).\nFOV: {}", fov)
            ));
        }

        if dir.length_sq() < EPSILON {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("dir")),
                &format!("Direction must not have zero length.\nDirection: {:?}", dir)
            ));
        }

        let dir_norm = dir.normalize();
        let dir_perp = dir_norm.perp_ccw();

        let fov2 = fov * 0.5;
        let perp_proj =  fov2.tan();    //Projection of ray dirs onto dir perpendicular, given, that projection on dir == 1.

        let mut l = Vector2D::from_basis(
            &dir_norm,
            &dir_perp,
            1.0,
            perp_proj
        );
        l.normalize_this();

        let mut r = Vector2D::from_basis(
            &dir_norm,
            &dir_perp,
            1.0,
            -perp_proj
        );
        r.normalize_this();

        Ok(View2D {
            l: Ray2D::new(origin, &l),
            r: Ray2D::new(origin, &r)
        })
    }

    pub fn origin(&self) -> &Point2D {
        &self.l.origin
    }

    pub fn clip_h_portal(&self, port_y: f32, port_min_x: f32, port_max_x: f32) -> Option<View2D> {
        let clip_l_pt = self.l.clip_h_line(port_y);
        let clip_r_pt = self.r.clip_h_line(port_y);

        if self.l.origin.y.approx_less(port_y) {
            let (clip_l_pt, clip_r_pt) = match (clip_l_pt, clip_r_pt) {
                (Some(l), Some(r)) => Some((l, r)),
                (Some(l), None) => Some((l, Point2D{ x: port_max_x, y: port_y})),
                (None, Some(r)) => Some((Point2D{ x: port_min_x, y: port_y }, r)),
                (None, None) => None
            }?;

            if clip_l_pt.x > clip_r_pt.x {
                return None;
            }

            if (clip_r_pt.x < port_min_x) || (clip_l_pt.x > port_max_x) {
                return None;
            }

            let new_l = if clip_l_pt.x < port_min_x {
                Ray2D::from_target(&self.l.origin, &clip_l_pt)
            } else {
                self.l.clone()
            };

            let new_r = if clip_r_pt.x > port_max_x {
                Ray2D::from_target(&self.l.origin, &clip_r_pt)
            } else {
                self.r.clone()
            };

            Some(View2D {
                l: new_l,
                r: new_r
            })
        } else {
            let (clip_l_pt, clip_r_pt) = match (clip_l_pt, clip_r_pt) {
                (Some(l), Some(r)) => Some((l, r)),
                (Some(l), None) => Some((l, Point2D{ x: port_min_x, y: port_y})),
                (None, Some(r)) => Some((Point2D{ x: port_max_x, y: port_y }, r)),
                (None, None) => None
            }?;

            if clip_l_pt.x < clip_r_pt.x {
                return None;
            }

            if (clip_l_pt.x < port_min_x) || (clip_r_pt.x > port_max_x) {
                return None;
            }

            let new_l = if clip_l_pt.x > port_max_x {
                Ray2D::from_target(&self.l.origin, &clip_l_pt)
            } else {
                self.l.clone()
            };

            let new_r = if clip_r_pt.x < port_min_x {
                Ray2D::from_target(&self.l.origin, &clip_r_pt)
            } else {
                self.r.clone()
            };

            Some(View2D {
                l: new_l,
                r: new_r
            })
        }


    }

    pub fn clip_v_portal(&self, port_x: f32, port_min_y: f32, port_max_y: f32) -> Option<View2D> {
        let clip_l_pt = self.l.clip_v_line(port_x);
        let clip_r_pt = self.r.clip_v_line(port_x);

        if self.l.origin.x.approx_less(port_x) {
            let (clip_l_pt, clip_r_pt) = match (clip_l_pt, clip_r_pt) {
                (Some(l), Some(r)) => Some((l, r)),
                (Some(l), None) => Some((l, Point2D{ x: port_x, y: port_min_y})),
                (None, Some(r)) => Some((Point2D{ x: port_x, y: port_max_y }, r)),
                (None, None) => None
            }?;

            if clip_l_pt.x < clip_r_pt.x {
                return None;
            }

            if (clip_l_pt.y < port_min_y) || (clip_r_pt.y > port_max_y) {
                return None;
            }

            let new_l = if clip_l_pt.y > port_max_y {
                Ray2D::from_target(&self.l.origin, &clip_l_pt)
            } else {
                self.l.clone()
            };

            let new_r = if clip_r_pt.y < port_min_y {
                Ray2D::from_target(&self.l.origin, &clip_r_pt)
            } else {
                self.r.clone()
            };

            Some(View2D {
                l: new_l,
                r: new_r
            })
        } else {
            let (clip_l_pt, clip_r_pt) = match (clip_l_pt, clip_r_pt) {
                (Some(l), Some(r)) => Some((l, r)),
                (Some(l), None) => Some((l, Point2D{ x: port_x, y: port_max_y})),
                (None, Some(r)) => Some((Point2D{ x: port_x, y: port_min_y }, r)),
                (None, None) => None
            }?;

            if clip_l_pt.x > clip_r_pt.x {
                return None;
            }

            if (clip_l_pt.y > port_max_y) || (clip_r_pt.y < port_min_y) {
                return None;
            }

            let new_l = if clip_l_pt.y < port_min_y {
                Ray2D::from_target(&self.l.origin, &clip_l_pt)
            } else {
                self.l.clone()
            };

            let new_r = if clip_r_pt.y > port_max_y {
                Ray2D::from_target(&self.l.origin, &clip_r_pt)
            } else {
                self.r.clone()
            };

            Some(View2D {
                l: new_l,
                r: new_r
            })
        }
    }
}