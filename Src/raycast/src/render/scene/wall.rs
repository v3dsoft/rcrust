use crate::math::{
    Point3D,
    EPSILON,
    F32Funcs
};

use super::WallDir;

pub enum SegmFill {
    Space,

    Wall {
        dir: WallDir
    }
}

pub struct Segment {
    pub height: f32,
    pub fill: SegmFill
}
type SegmList = Vec<Segment>;

pub struct Wall {
    pub origin: Point3D,
    pub segm_list: SegmList
}

impl Wall {
    pub fn simple(origin: &Point3D, dir: WallDir, height: f32) -> Self {
        debug_assert!(height > EPSILON, "Wall height must greater than 0.");

        let mut segm_list = Vec::with_capacity(1);
        segm_list.push(Segment{
            height,
            fill: SegmFill::Wall {
                dir
            }
        });

        Self {
            origin: origin.clone(),
            segm_list
        }
    }

    pub fn build(origin: &Point3D, dir: WallDir) -> WallBuilder {
        WallBuilder::new(origin, dir)
    }
}

pub struct WallBuilder {
    wall: Wall,

    dir: WallDir,
    rev_dir: WallDir,

    curr_height: f32
}

impl WallBuilder {
    fn new(origin: &Point3D, dir: WallDir) -> Self {
        let wall = Wall {
            origin: origin.clone(),
            segm_list: Vec::new()
        };

        let rev_dir = dir.reversed();

        Self {
            wall,

            dir,
            rev_dir,

            curr_height: 0.0
        }
    }

    pub fn add_segment_height(&mut self, height: f32, reversed: bool) {
        debug_assert!(height > EPSILON, "Wall segment height must be greater than 0.");

        let dir = if reversed { self.rev_dir } else { self.dir };
        self.wall.segm_list.push(Segment {
            height,
            fill: SegmFill::Wall {
                dir
            }
        });

        self.curr_height += height;
    }

    pub fn add_segment_top(&mut self, top: f32, reversed: bool) {
        let curr_top = self.wall.origin.z + self.curr_height;
        debug_assert!(curr_top.approx_less(top), "Top coordinate must be above current wall height.");

        self.add_segment_height(top - curr_top, reversed);
    }

    pub fn add_space_height(&mut self, height: f32) {
        debug_assert!(height > EPSILON, "Wall space height must be greater than 0.");

        self.wall.segm_list.push(Segment {
            height,
            fill: SegmFill::Space
        });

        self.curr_height += height;
    }

    pub fn add_space_top(&mut self, top: f32) {
        let curr_top = self.wall.origin.z + self.curr_height;
        debug_assert!(curr_top.approx_less(top), "Top coordinate must be above current wall height.");

        self.add_space_height(top - curr_top);
    }

    pub fn create(self) -> Wall {
        self.wall
    }
}