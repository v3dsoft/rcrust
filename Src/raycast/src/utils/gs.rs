use crate::error;

use super::time::Clock;

const DEFAULT_TIME_SLICE: u32 = 1000 / 60;  //60 FPS

pub trait GameState {
    fn update(&mut self, time_delta: u32) -> GSChange;
    fn render(&mut self) -> error::Result<()>;
}
pub type OptionalGS = Option<Box<dyn GameState>>;

pub enum GSChange {
    Same,
    New(OptionalGS)
}

pub struct GSProcessor {
    state: OptionalGS,

    clock: Clock,
    time_slice: u32,
    unproc_time: u32
}

impl GSProcessor {
    pub fn new() -> GSProcessor {
        GSProcessor {
            state: None,

            clock: Clock::new(),
            time_slice: DEFAULT_TIME_SLICE,
            unproc_time: 0
        }
    }

    pub fn with_slice(slice: u32) -> GSProcessor {
        if slice == 0 {
            panic!("Time slice must be greater than 0.");
        }

        GSProcessor {
            state: None,

            clock: Clock::new(),
            time_slice: slice,
            unproc_time: 0
        }
    }

    pub fn set_state(&mut self, state: OptionalGS) {
        self.state = state;
    }

    pub fn state(&self) -> &OptionalGS {
        &self.state
    }

    pub fn update(&mut self) -> bool {
        self.clock.update();

        if self.state.is_some() {
            let mut time_to_proc = self.unproc_time + self.clock.time_ms();
            while (time_to_proc > self.time_slice) && self.state.is_some() {
                let mut change = GSChange::Same;

                if let Some(ref mut state) = self.state {
                    change = state.update(self.time_slice);
                }

                if let GSChange::New(new_state) = change {
                    self.state = new_state;
                }

                time_to_proc -= self.time_slice;
            }

            self.unproc_time = time_to_proc;
        }

        self.clock.reset();

        self.state.is_some()
    }

    pub fn render(&mut self) -> error::Result<()> {
        if let Some(ref mut state) = self.state {
            state.render()
        } else {
            Ok(())
        }
    }
}