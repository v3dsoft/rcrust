use sdl2;

use crate::error;

struct SdlGlobData {
    sdl: Option<sdl2::Sdl>,
    ref_count: u32
}

static mut SDL_GLOB_DATA: SdlGlobData = SdlGlobData {
    sdl: None,
    ref_count: 0
};

impl SdlGlobData {
    pub fn acquire(&mut self) -> error::Result<()> {
        if self.ref_count > 0 {
            self.ref_count += 1;

            Ok(())
        } else {
            let sdl = match sdl2::init() {
                Ok(sdl) => sdl,
                Err(sdl_err) => {
                    return Err(error::Error::new(error::Cause::Internal, &format!("Cannot initialize SDL.\nError: {}", sdl_err)))
                }
            };

            self.sdl = Some(sdl);
            self.ref_count = 1;

            Ok(())
        }
    }

    pub fn release(&mut self) {
        if self.ref_count > 1 {
            self.ref_count -= 1;
        } else {
            self.sdl = None;
            self.ref_count = 0;
        }
    }

    pub fn get_instance(&self) -> &sdl2::Sdl {
        match &self.sdl {
            Some(ref sdl) => sdl,
            None => panic!("Attempt to get SDL instance before initialization.")
        }
    }
}

pub struct SdlInstance {}

impl SdlInstance {
    pub fn new() -> error::Result<SdlInstance> {
        unsafe {
            SDL_GLOB_DATA.acquire()?;
        }

        Ok(SdlInstance {})
    }

    pub fn init_video(&self) -> error::Result<sdl2::VideoSubsystem> {
        unsafe {
            match SDL_GLOB_DATA.get_instance().video() {
                Ok(video) => Ok(video),
                Err(sdl_err) => Err(error::Error::new(
                    error::Cause::Internal,
                    &format!("Cannot initialize SDL video subsystem.\nError: {}", sdl_err)
                ))
            }
        }
    }

    pub fn init_event_pump(&self) -> error::Result<sdl2::EventPump> {
        unsafe {
            match SDL_GLOB_DATA.get_instance().event_pump() {
                Ok(event_pump) => Ok(event_pump),
                Err(sdl_err) => Err(error::Error::new(
                    error::Cause::Internal,
                    &format!("Cannot initialize SDL event pump.\nError: {}", sdl_err)
                ))
            }
        }
    }
}

impl Drop for SdlInstance {
    fn drop(&mut self) {
        unsafe {
            SDL_GLOB_DATA.release();
        }
    }
}