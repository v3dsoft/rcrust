mod registry;
pub use self::registry::ResRegistry;

mod guard;
pub use self::guard::ResGuard;

mod generation_id;
pub use self::generation_id::GenerationId;

mod generation_registry;
pub use self::generation_registry::GenerationResRegistry;