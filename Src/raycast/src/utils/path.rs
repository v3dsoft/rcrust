pub fn extract_ext(path: &str) -> String {
    match path.rfind('.') {
        Some(start_pos) => {
            let ext = String::from(&path[start_pos..]);
            ext.to_lowercase()
        },

        None => String::new()
    }
}