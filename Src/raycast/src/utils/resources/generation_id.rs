use std::{ u32, u16 };
use std::sync::atomic::{ AtomicU16, Ordering };

static NEXT_GEN_ID_POOL_ID: AtomicU16 = AtomicU16::new(0);

#[derive(Copy, Clone, Hash)]
pub struct GenerationId {
    id: u32,

    owner: u16,
    generation: u16
}

impl PartialEq for GenerationId {
    fn eq(&self, other: &Self) -> bool {
        (self.id == other.id) && (self.owner == other.owner) && (self.generation == other.generation)
    }
}

impl Eq for GenerationId {}

pub struct GenerationIdPool {
    id: u16,
    next_id: u32,
    free_id_list: Vec<GenerationId>
}

impl GenerationIdPool {
    pub fn new() -> Self {
        let id = NEXT_GEN_ID_POOL_ID.fetch_add(1, Ordering::SeqCst);
        if id == u16::MAX {
            panic!("We're out of free pool ID's.");
        }

        Self {
            id,
            next_id: 0,
            free_id_list: Vec::new()
        }
    }

    fn pop_free_id(&mut self) -> Option<GenerationId> {
        if let Some(mut free_id) = self.free_id_list.pop() {
            free_id.generation += 1;

            Some(free_id)
        } else {
            None
        }
    }

    fn push_free_id(&mut self, id: GenerationId) {
        if id.generation < u16::MAX {
            self.free_id_list.push(id);
        }
    }

    fn get_new_id(&mut self) -> GenerationId {
        if self.next_id == u32::MAX {
            panic!("Pool {} is out of free ID's.", self.id);
        }

        let new_id = GenerationId {
            id: self.next_id,

            owner: self.id,
            generation: 0
        };

        self.next_id += 1;

        new_id
    }

    pub fn acquire_id(&mut self) -> GenerationId {
        if let Some(id) = self.pop_free_id() {
            id
        } else {
            self.get_new_id()
        }
    }

    pub fn release_id(&mut self, id: GenerationId) {
        self.check_id_owner(id);
        self.push_free_id(id);
    }

    pub fn check_id_owner(&self, id: GenerationId) {
        if id.owner != self.id {
            panic!("Wrong id owner.\nMy ID: {}\nID owner: {}", self.id, id.owner);
        }
    }
}