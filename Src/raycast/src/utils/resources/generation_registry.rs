use std::collections::HashMap;

use crate::error;
use super::generation_id::{ GenerationId, GenerationIdPool };

struct ResRecord<Item> {
    item: Item,
    ref_count: u32
}

pub struct GenerationResRegistry<Item> {
    id_pool: GenerationIdPool,
    data: HashMap<GenerationId, ResRecord<Item>>
}

impl<Item> GenerationResRegistry<Item> {
    pub fn new() -> Self {
        let id_pool = GenerationIdPool::new();
        let data = HashMap::new();

        Self {
            id_pool,
            data
        }
    }

    pub fn insert_item(&mut self, item: Item) -> GenerationId {
        let item_id = self.id_pool.acquire_id();
        let item_rec = ResRecord {
            item,
            ref_count: 1
        };

        self.data.insert(item_id, item_rec);

        item_id
    }

    pub fn acquire_item(&mut self, id: GenerationId) -> error::Result<u32> {
        self.id_pool.check_id_owner(id);

        if let Some(item_rec) = self.data.get_mut(&id) {
            item_rec.ref_count += 1;

            Ok(item_rec.ref_count)
        } else {
            Err(error::Error::new(
                error::Cause::WrongArgument(String::from("id")),
                "Wrong item ID."
            ))
        }
    }

    pub fn item_exists(&self, id: GenerationId) -> bool {
        self.id_pool.check_id_owner(id);

        self.data.get(&id).is_some()
    }

    pub fn release_item(&mut self, id: GenerationId) -> error::Result<u32> {
        self.id_pool.check_id_owner(id);

        if let Some(item_rec) = self.data.get_mut(&id) {
            item_rec.ref_count -= 1;

            if item_rec.ref_count > 0 {
                Ok(item_rec.ref_count)
            } else {
                self.data.remove(&id);
                self.id_pool.release_id(id);

                Ok(0)
            }
        } else {
            Err(error::Error::new(
                error::Cause::WrongArgument(String::from("id")),
                "Wrong item ID."
            ))
        }
    }

    pub fn borrow_item(&self, id: GenerationId) -> error::Result<&Item> {
        self.id_pool.check_id_owner(id);

        if let Some(item_rec) = self.data.get(&id) {
            Ok(&item_rec.item)
        } else {
            Err(error::Error::new(
                error::Cause::WrongArgument(String::from("id")),
                "Wrong item ID."
            ))
        }
    }

    pub fn borrow_item_mut(&mut self, id: GenerationId) -> error::Result<&mut Item> {
        self.id_pool.check_id_owner(id);

        if let Some(item_rec) = self.data.get_mut(&id) {
            Ok(&mut item_rec.item)
        } else {
            Err(error::Error::new(
                error::Cause::WrongArgument(String::from("id")),
                "Wrong item ID."
            ))
        }
    }
}