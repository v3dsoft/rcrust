use std::marker::PhantomData;

use crate::error;

const INITIAL_CAPACITY: usize = 64;

struct ResRecord<Item> {
    item: Item,
    ref_count: u32
}

pub struct ResRegistry<Key, Item> {
    data: Vec<Option<ResRecord<Item>>>,
    _key_marker: PhantomData<Key>
}

impl<Key: From<usize> + Into<usize>, Item> ResRegistry<Key, Item> {
    pub fn new() -> Self {
        Self {
            data: Vec::with_capacity(INITIAL_CAPACITY),
            _key_marker: PhantomData
        }
    }

    fn find_vacant_item(&self) -> Option<usize> {
        let data_len = self.data.len();
        for i in 0..data_len {
            if self.data[i].is_none() {
                return Some(i);
            }
        }

        None
    }

    pub fn insert_item(&mut self, item: Item) -> Key {
        let item_rec = ResRecord {
            item,
            ref_count: 1
        };

        match self.find_vacant_item() {
            Some(item_id) => {
                self.data[item_id] = Some(item_rec);

                Key::from(item_id)
            },

            None => {
                let item_id = self.data.len();
                self.data.push(Some(item_rec));

                Key::from(item_id)
            }
        }
    }

    pub fn acquire_item(&mut self, item_id: Key) -> error::Result<u32> {
        let item_id: usize = item_id.into();

        if item_id >= self.data.len() {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("There is no item with this ID.\nItem ID: {}", item_id)
            ));
        }

        match &mut self.data[item_id] {
            Some(item_rec) => {
                item_rec.ref_count += 1;

                Ok(item_rec.ref_count)
            },

            None => Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("Item with this ID is vacant.\nItem ID: {}", item_id)
            ))
        }
    }

    pub fn release_item(&mut self, item_id: Key) -> error::Result<u32> {
        let item_id: usize = item_id.into();

        if item_id >= self.data.len() {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("There is no item with this ID.\nItem ID: {}", item_id)
            ));
        }

        match &mut self.data[item_id] {
            Some(item_rec) => {
                item_rec.ref_count -= 1;
                if item_rec.ref_count == 0 {
                    self.data[item_id] = None;

                    Ok(0)
                } else {
                    Ok(item_rec.ref_count)
                }
            },

            None => Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("Item with this ID is vacant.\nItem ID: {}", item_id)
            ))
        }
    }

    pub fn borrow_item(&self, item_id: Key) -> error::Result<&Item> {
        let item_id: usize = item_id.into();

        if item_id >= self.data.len() {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("There is no item with this ID.\nItem ID: {}", item_id)
            ));
        }

        match &self.data[item_id] {
            Some(item_rec) => Ok(&item_rec.item),

            None => Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("Item with this ID is vacant.\nItem ID: {}", item_id)
            ))
        }
    }

    pub fn borrow_item_mut(&mut self, item_id: Key) -> error::Result<&mut Item> {
        let item_id: usize = item_id.into();

        if item_id >= self.data.len() {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("There is no item with this ID.\nItem ID: {}", item_id)
            ));
        }

        match &mut self.data[item_id] {
            Some(item_rec) => Ok(&mut item_rec.item),

            None => Err(error::Error::new(
                error::Cause::WrongArgument(String::from("item_id")),
                &format!("Item with this ID is vacant.\nItem ID: {}", item_id)
            ))
        }
    }

    pub fn try_borrow_item(&self, item_id: Key) -> Option<&Item> {
        let item_id: usize = item_id.into();

        if item_id >= self.data.len() {
            return None;
        }

        match &self.data[item_id] {
            Some(item_rec) => Some(&item_rec.item),
            None => None
        }
    }
}