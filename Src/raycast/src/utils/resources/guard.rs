pub struct ResGuard<T, DropFn: FnMut(T)> {
    item: Option<T>,
    drop_fn: DropFn
}

impl<T, DropFn: FnMut(T)> ResGuard<T, DropFn> {
    pub fn new(item: T, drop_fn: DropFn) -> Self {
        Self {
            item: Some(item),
            drop_fn
        }
    }

    pub fn borrow(&self) -> &T {
        if let Some(item) = &self.item {
            item
        } else {
            panic!("Resource guard has no item stored.");
        }
    }

    pub fn borrow_mut(&mut self) -> &mut T {
        if let Some(item) = &mut self.item {
            item
        } else {
            panic!("Resource guard has no item stored.");
        }
    }

    pub fn take(mut self) -> T {
        self.item.take().expect("Resource guard has no item stored.")
    }
}

impl<T, DropFn: FnMut(T)> Drop for ResGuard<T, DropFn> {
    fn drop(&mut self) {
        if let Some(item) = self.item.take() {
            (self.drop_fn)(item);
        }
    }
}