use std::time::Instant;

pub struct Clock {
    start: Instant,
    time: u32
}

impl Clock {
    pub fn new() -> Clock {
        Clock {
            start: Instant::now(),
            time: 0
        }
    }

    pub fn reset(&mut self) {
        self.start = Instant::now();
        self.time = 0;
    }

    pub fn update(&mut self) {
        let elapsed = self.start.elapsed();

        //TODO: Make this simpler when appropriate API will land in stable
        self.time = (elapsed.as_secs() as u32) * 1000;
        self.time += elapsed.subsec_millis();
    }

    pub fn time_ms(&self) -> u32 {
        self.time
    }
}