use crate::error;
use crate::math::{
    Point3D,
    Vector3D,

    linear_coord
};

use crate::engine::EngineRC;

use crate::render::scene::{
    RenderScene,
    CellFill,
    RenderPlane,

    Camera
};

pub const CELL_SIZE: f32 = 200.0;

pub enum Cell {
    Wall,
    Space {
        floor: f32,
        ceil: f32
    }
}

pub struct Scene {
    engine: EngineRC,

    width: u32,
    height: u32,

    cell_list: Vec<Cell>,

    render_scene: RenderScene
}

impl Scene {
    fn new(engine: EngineRC, width: u32, height: u32) -> error::Result<Self> {
        if width == 0 {
            return Err(error::Error::new(error::Cause::WrongArgument(String::from("width")), "Map width must be greater than 0."));
        }

        if height == 0 {
            return Err(error::Error::new(error::Cause::WrongArgument(String::from("height")), "Map height must be greater than 0."));
        }

        let cell_count = width * height;
        let mut cell_list = Vec::with_capacity(cell_count as usize);
        for _cell in 0..cell_count {
            cell_list.push(Cell::Wall);
        }

        let render_scene = RenderScene::new(width, height, CELL_SIZE)?;

        Ok(Self {
            engine,

            width,
            height,

            cell_list,

            render_scene
        })
    }

    fn check_cell(&self, x: u32, y: u32) -> bool {
        (x < self.width) && (y < self.height)
    }

    fn set_cell(&mut self, x: u32, y: u32, cell: Cell) -> error::Result<()> {
        if !self.check_cell(x, y) {
            return Err(error::Error::new(
                error::Cause::WrongArgument(String::from("x, y")),
                &format!("Cell coordinates are out of scene bounds.\nCoords: {}:{}", x, y)
            ));
        }

        let cell_index = linear_coord(x, y, self.width);
        self.cell_list[cell_index as usize] = cell;

        Ok(())
    }

    fn update_render_scene(&mut self) -> error::Result<()> {
        let mut updater = self.render_scene.begin_update();

        let mut cell_index: usize = 0;
        for y in 0..self.height {
            for x in 0..self.width {
                match self.cell_list[cell_index] {
                    Cell::Wall => {
                        updater.set_cell(x, y, CellFill::Wall)?;
                    },

                    Cell::Space {floor, ceil} => {
                        updater.set_cell(x, y, CellFill::Space {
                            floor: RenderPlane { height: floor },
                            ceiling: RenderPlane { height: ceil }
                        })?;
                    }
                }

                cell_index += 1;
            }
        }

        Ok(())
    }

    pub fn create_camera(&self, pos: &Point3D, h_angle: f32, v_angle: f32, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        self.render_scene.create_camera(pos, h_angle, v_angle, fov_v_angle, aspect)
    }

    pub fn create_camera_dir(&self, pos: &Point3D, dir: &Vector3D, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        self.render_scene.create_camera_dir(pos, dir, fov_v_angle, aspect)
    }

    pub fn create_camera_target(&self, pos: &Point3D, tgt: &Point3D, fov_v_angle: f32, aspect: f32) -> error::Result<Camera> {
        self.render_scene.create_camera_target(pos, tgt, fov_v_angle, aspect)
    }

    pub fn render(&self, camera: &Camera) -> error::Result<()> {
        self.render_scene.render(self.engine.borrow_mut().render_driver_mut(), camera)
    }
}

impl Drop for Scene {
    fn drop(&mut self) {
        self.render_scene.release_resources(self.engine.borrow_mut().render_driver_mut());
    }
}

pub struct SceneBuilder {
    scene: Scene
}

impl SceneBuilder {
    pub(crate) fn new(engine: EngineRC, width: u32, height: u32) -> error::Result<Self> {
        let scene = Scene::new(engine, width, height)?;

        Ok(SceneBuilder{
            scene
        })
    }

    pub fn set_cell(&mut self, x: u32, y: u32, cell: Cell) -> error::Result<()> {
        self.scene.set_cell(x, y, cell)
    }

    pub fn create(self) -> error::Result<Scene> {
        let mut scene = self.scene;
        scene.update_render_scene()?;

        Ok(scene)
    }
}